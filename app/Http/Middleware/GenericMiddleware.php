<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App\Model\TNotificacion;

class GenericMiddleware
{
    public function handle($request, Closure $next)
    {
    	$urlRequest=$request->url();


    	$urlBase=env('URL_BASE_FILTER');
           
    //    if($urlBase=='http://www.ugelgrau.gob.pe')
    //    {
    //        $urlBase='http://www.ugelgrau.gob.pe'
    //    }

//Session::flush();

    	$filterRol=explode(',', Session::get('rol', 'Anónimo'));
        //DD($filterRol);exit;
    	$urlAccess=
    	[

    		[$urlBase, 'superusuario,Anónimo', false],
    		[$urlBase.'/usuario/login','Anónimo,superusuario',false],
            [$urlBase.'/usuario/logout','superusuario',false],
    		[$urlBase.'/usuario/index', 'superusuario', false],
            [$urlBase.'/usuario/registar','superusuario',false],
    		[$urlBase.'usuario/insert','superusuario',false],
            [$urlBase.'/usuario/editar','superusuario',false],
            [$urlBase.'/usuario/cambiaravatar','superusuario',false],


            [$urlBase.'/index','superusuario',false],
            [$urlBase.'/servicio/index','superusuario',false],
            [$urlBase.'/servicio/insert', 'superusuario', false],
            [$urlBase.'/usuario/registar','superusuario',false],
            [$urlBase.'/servicio/edit','superusuario',false],
            [$urlBase.'/servicio/eliminar','superusuario',true],
            [$urlBase.'/publicacion/index','superusuario',false],

            [$urlBase.'/publicacion/insert','superusuario',false],
            [$urlBase.'/publicacion/todas','Anónimo',false],
            [$urlBase.'/publicacion/vermas','Anónimo',true],
            [$urlBase.'/publicacion/index',',superusuario',false],
            [$urlBase.'/bienvenida', 'Anónimo', false],
            [$urlBase.'/area/insert','superusuario',false],
            [$urlBase.'/area/index','superusuario',false],
            [$urlBase.'/area/edit','superusuario',false],
            [$urlBase.'/area/toblock','superusuario',false],

            [$urlBase.'/directorio','Anónimo',false],
            [$urlBase.'/directorio/index','superusuario,Anónimo',false],
            [$urlBase.'/directorio/insert', 'superusuario', false],
            [$urlBase.'/slider/insert','superusuario',false],
            [$urlBase.'/slider/index','superusuario',false],

            [$urlBase.'/anuncio/insert','superusuario',false],
            [$urlBase.'/anuncio/index','superusuario',false],
            [$urlBase.'/anuncio/edit','superusuario',false],

    	];

        $access=false;

        foreach($urlAccess as $key => $value)
        {
        	if($value[0]==$urlRequest || ($value[2] && strpos($urlRequest, $value[0])===0))
        	{
        		foreach($filterRol as $index => $item)
        		{
        			if(strpos($value[1], $item)!==false)
	        		{
	        			$access=true;

	        			break 2;
	        		}
        		}
        	}
        }

        if(!$access)
        {
        	if($request->ajax())
        	{
        		exit;
        	}
            //Session::flush();

        	return redirect('/');
        }


        return $next($request);
    }
}
?>
