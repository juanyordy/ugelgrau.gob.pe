create  table tarea
(
codigoarea char(13) not null primary key,
nombre varchar(700) not null,
descripcion text not null,
activo bool,
fecharegistro datetime,
fechamodificacion datetime
);

create table tdirectorio
(
codigodirectorio char(13) not null,
codigousuario char(13) not null,
nombre varchar(70) not null,
nombreapellido varchar(100),
correoelectronico varchar(700),
fecharegistro datetime,
fechamodificacion datetime,
primary key(codigodirectorio),
foreign key (codigoUsuario) references tusuario(codigousuario)

);

create table tbienvenida
(
	codigobienvenida char(13) not null,
	codigousuario char(13) not null,
	perido datetime not null,
	descripcion text not null,
	extencion char(15) not null,
	estado bool not null,
	fecharegistro datetime,
	fechamodificacion datetime,
	primary key (codigobienvenida),
	foreign key (codigousuario) references tusuario(codigousuario)
);

create table tslider
(
codigoslider char(13) not null,
codigousuario char(13) not null,
titulo varchar(100) not null,
descripcion text not null,
extensionslider char(15) not null,
estado bool,
fecharegistro datetime,
fechamodificacion datetime,
primary key (codigoslider),
foreign key (codigousuario) references tusuario(codigousuario)
);

create table tanuncio
(
codigoanuncio char(13) not null,
codigousuario char(13) not null,
titulo varchar(100) not null,
descripcion text not null,
extensionanuncio char(5) not null,
estado bool,
importante bool,
fecharegistro datetime,
fechamodificacion datetime,
primary key (codigoanuncio),
foreign key (codigousuario) references tusuario(codigousuario)
);
