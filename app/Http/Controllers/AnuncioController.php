<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use App\Model\TAnuncio;
use DB;

class AnuncioController extends Controller
{
	public function actionIndex()
	{
		$tAnuncio=TAnuncio::all();

		return view('anuncio/index',['tanuncios'=>$tAnuncio]);
	}

	public function actionInsert(Request $request,SessionManager $sesionManager)
	{
		if($_POST)
		{
		try
			{
				DB::beginTransaction();

				$tAnuncio=new TAnuncio();
 				$codigoAnuncio=uniqid();

 			
				$fileGetClientOriginalExtension=strtolower($request->file('fileExtencion')->getClientOriginalExtension());
				$rutaAvatarActual='imagenes/anuncio/'.$codigoAnuncio.'.'.$fileGetClientOriginalExtension;
			
				if(file_exists($rutaAvatarActual))
				{
					unlink($rutaAvatarActual);
				}
				$request->file('fileExtencion')->move('imagenes/anuncio/',$codigoAnuncio.'.'.$fileGetClientOriginalExtension);


				$tAnuncio->codigoAnuncio=$codigoAnuncio;
				$tAnuncio->codigousuario=$sesionManager->get('codigousuario');
				$tAnuncio->titulo=$request->input('txtTitulo');
				$tAnuncio->descripcion=$request->input('txtDescripcion');
				$tAnuncio->link=$request->input('txtLink');
				$tAnuncio->importante=false;
				$tAnuncio->estado=true;

				$tAnuncio->extensionanuncio=$fileGetClientOriginalExtension;
			


			$tAnuncio->save();
			DB::commit();
			} 
			catch (Exception $e)
			{
				
			}
		}
		return view('anuncio/insert');
	}

	public function actionEdit(Request $request)
	{
		if($request->has('hdEdit'))
		{

		}
		$tanuncio=TAnuncio::find($request->input('codigoanuncio'));
		return view('anuncio/edit',['tanuncio'=>$tanuncio]);
	}
}

?>