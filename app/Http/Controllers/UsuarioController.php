<?php
namespace App\Http\Controllers;
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
use Validator;
use App\Model\TUsuario;
use DB;


class UsuarioController extends Controller
{
	
	public function actionIndex()
	{
		return view('index');
	}

	public function actionInsert(Request $request,Encrypter $encrypter, SessionManager $sessionManager)
	{

		if($_POST)
		{
			$validator=Validator::make(
			[
				'Nombre' => trim($request->input('txtNombre')),
				'Apellido' => trim($request->input('txtApellido')),
				'Correo electrónico' => trim($request->input('txtCorreoElectronico')),
				'Contraseña' => $request->input('passContrasenia')
			],
			[
				'Nombre' => ['required'],
				'Apellido' => ['required'],
				'Correo electrónico' => ['required', 'email', 'unique:tusuario,correoElectronico'],
				'Contraseña' => ['required']
			],
			[
				'required' => 'El campo ":Attribute" es requerido.<br>',
				'email' => 'El formato del campo ":Attribute" es incorrecto.<br>',
				'unique' => 'El ":Attribute" ya se encuentra registrado en el sistema.<br>'
			]);

			$mensajeGlobal='';

			if($validator->fails())
			{
				$errors=$validator->errors()->all();

				foreach($errors as $value)
				{
					$mensajeGlobal.=$value;
				}
			}

			if($request->input('passContrasenia')!=$request->input('passContraseniaRepita'))
			{
				$mensajeGlobal.='Las contraseñas no coinciden.<br>';
			}

			if($mensajeGlobal!='')
			{
				$request->flash();

				$sessionManager->flash('mensajeGlobal', $mensajeGlobal);
				$sessionManager->flash('correcto', 'No');
				
				return redirect('/usuario/insert');
			}
			try 
			{
				$codigoUsuario=uniqid();

				copy(public_path().'/usuario/avatar/user.png', public_path().'/usuario/avatar/'.$codigoUsuario.'.png');

				$tUsuario=new TUsuario();

				$tUsuario->codigousuario=$codigoUsuario;
				
				$tUsuario->correoelectronico=$request->input('txtCorreoElectronico');
				$tUsuario->contrasenia=$encrypter->encrypt($request->input('passContrasenia'));
				$tUsuario->nombre=$request->input('txtNombre');
				$tUsuario->apellido=$request->input('txtApellido');
				$tUsuario->fechanacimiento='1991-01-01';
				$tUsuario->dni=trim('12345678');
				$tUsuario->direccion='Salazar Bondy';
				$tUsuario->telefono='986145234';
				$tUsuario->sexo=true;
				$tUsuario->rol='Usuario normal';
				$tUsuario->estado='activo';
				$tUsuario->extensionavatar='png';
	
				$tUsuario->save();
				DB::commit();
				$mensajeGlobal='Fuiste registrado(a) correctamente en el sistema';
							
			} 
			catch (Exception $e) 
			{ 
				DB::rollback();
            	$mensajeGlobal='OCurrio un error inesperado';
				
			}

			$sessionManager->flash('mensajeGlobal',$mensajeGlobal);
			$sessionManager->flash('correcto', 'Si');	

		
		}
			return view('usuario/insert');
	}

	public function actionLogin(Request $request,Encrypter $encrypter,SessionManager $sessionManager)
	{
		if($_POST)
		{
			$contrasenia=$request->input('passContrasenia');

			$validator=Validator(

			[
				'Email'=>trim($request->input('txtCorreoElectronico')),
				'Contraseña'=>trim($request->input('passContrasenia'))
			],

			[
				'Email'=>['required','email'],
				'Contraseña'=>['required']
			],
			[
				'required'=>'el campo":Attribute" es Requeriso',
				'email'=>'el campo ":Attribute" no cumple el formato',
			]);

			$mensajeGlobal='';

			if($validator->fails())
			{
				$errors=$validator->errors()->all();

				foreach($errors as $value)
				{
					$mensajeGlobal.=$value;
				}
			}

			if($mensajeGlobal!='')
			{
				$request->flash();

				$sessionManager->flash('mensajeGlobal', $mensajeGlobal);
				$sessionManager->flash('correcto', 'No');
				
				return redirect('/usuario/login');
			}

			$tUsuario=TUsuario::where('correoelectronico',$request->input('txtCorreoElectronico'))->first();
			
			if($tUsuario==null)
			{	
				$mensajeGlobal='Datos Incorreectos';

				$sessionManager->flash('mensajeGlobal', $mensajeGlobal);
				$sessionManager->flash('correcto', 'No');
				
				return redirect('usuario/login');
			}
			if($encrypter->decrypt($tUsuario->contrasenia)!=$contrasenia)
			{
				$mensajeGlobal='Datos Incorreectos';

				$sessionManager->flash('mensajeGlobal', $mensajeGlobal);
				$sessionManager->flash('correcto', 'No');
				
				return redirect('usuario/login');
			}

			$sessionManager->put('codigousuario',$tUsuario->codigousuario);
		
			$sessionManager->put('correoelectonico',$tUsuario->correoelectronico);
			//$sessionManager->put('rol','superusuario');
			$sessionManager->put('rol',$tUsuario->rol);
			$sessionManager->put('nombre', $tUsuario->nombre);
			$sessionManager->put('apellido', $tUsuario->apellido);
			$sessionManager->put('extensionAvatar',$tUsuario->extensionavatar);
   
   

			return redirect('/index');
		}

		return View('usuario/login');


	}

	public function actionLogout(SessionManager $sessionManager)
	{

		$sessionManager->flush();

		return redirect('/usuario/login');
	}
	public function actionEdit(Request $request,SessionManager $sessionManager)
	{
		
		if($_POST)
		{
			$validator=Validator::make(
			[
				'Nombre' => trim($request->input('txtNombre')),
				'Apellido' => trim($request->input('txtApellido')),
				'Correo electrónico' => trim($request->input('txtCorreoElectronico')),
				'Fecha de nacimiento' => trim($request->input('dateFechaNacimiento')),
				'Dni' => trim($request->input('txtDni'))
			],
			[
				'Nombre' => ['required'],
				'Apellido' => ['required'],
				'Correo electrónico' => ['required', 'email', 'unique:usuario,correoElectronico,'.$sessionManager->get('codigoUsuario').',codigoUsuario'],
				'Fecha de nacimiento' => ['required', 'regex:/^((((1|2){1}\d{3}\/(0|1){1}\d{1}\/[0-3]{1}\d{1})|((1|2){1}\d{3}\-(0|1){1}\d{1}\-[0-3]{1}\d{1})){1})*$/'],
				'Dni' => ['regex:/^[0-9]{8}$/']
			],
			[
				'required' => 'El campo ":Attribute" es requerido.<br>',
				'email' => 'El formato del campo ":Attribute" es incorrecto.<br>',
				'unique' => 'El ":Attribute" ya se encuentra registrado en el sistema.<br>',
				'regex' => 'El formato del campo ":Attribute" es incorrecto.<br>',
			]);

			$mensajeGlobal='';

			if($validator->fails())
			{
				$errors=$validator->errors()->all();

				foreach($errors as $value)
				{
					$mensajeGlobal.=$value;
				}
			}

			if($mensajeGlobal!='')
			{
				$sessionManager->flash('mensajeGlobal', $mensajeGlobal);
				$sessionManager->flash('correcto', false);
				
				return redirect('/usuario/editar');
			}

			$tUsuario=TUsuario::find($sessionManager->get('codigoUsuario'));

			$tUsuario->nombre=trim($request->input('txtNombre'));
			$tUsuario->apellido=trim($request->input('txtApellido'));
			$tUsuario->correoElectronico=trim($request->input('txtCorreoElectronico'));
			$tUsuario->dni=trim($request->input('txtDni'));
			$tUsuario->fechaNacimiento=trim($request->input('dateFechaNacimiento'));
			$tUsuario->sexo=($request->input('radioSexo')=='M' ? true : false);

			$tUsuario->save();

			$sessionManager->flash('mensajeGlobal', 'Datos guardados correctamente.');
			$sessionManager->flash('correcto', true);

			return redirect('/usuario/editar');
		}

		$tUsuario=TUsuario::find($sessionManager->get('codigoUsuario'));

		return View('usuario/edit',['tUsuario'=>$tUsuario]);

	}

	public function actionCambiarAvatar(Request $request,SessionManager $sessionManager)
	{
		if($request->has('hdPost'))
		{
			if(!($request->hasfile('fileAvatar')))
			{
				$sessionManager->flash('mensajeGlobal','no se Seleciono ninguna foto');
				$sessionManager->flash('correcto',false);

				return redirect('/usuario/editar');		
			}

			$fileGetClientOriginalExtension=strtolower($request->file('fileAvatar')->getClientOriginalExtension());
			$fileGetSizeKb=($request->file('fileAvatar')->getSize()/1024);

			if($fileGetClientOriginalExtension!='jpg'&&$fileGetClientOriginalExtension!='png'&&$fileGetClientOriginalExtension!='jpeg')
			{
				$sessionManager->flash('mensajeGlobal', 'El formato del archivo sólo puede ser jpg, jpeg o png.');
				$sessionManager->flash('correcto', false);

				return redirect('/usuario/editar');
			}
			if($fileGetSizeKb>3075)
			{
				$sessionManager->flash('mensajeGlobal', 'El tamaño del archivo no puede ser mayor a 3mb.');
				$sessionManager->flash('correcto', false);

				return redirect('/usuario/editar');
			}

			$tUsuario=TUsuario::find($sessionManager->get('codigoUsuario'));

			$rutaActualAvatar=public_path().'/avatar/'.$tUsuario->codigoUsuario.'.'.$tUsuario->extensionAvatar;
			
			$tUsuario->extensionAvatar=$fileGetClientOriginalExtension;

			if(file_exists($rutaActualAvatar))
			{
				unlink($rutaActualAvatar);
			}

			$request->file('fileAvatar')->move(public_path().'/avatar',$sessionManager->get('codigoUsuario').'.'.$fileGetClientOriginalExtension);

			$tUsuario->save();
			$sessionManager->put('extensionAvatar',$tUsuario->extensionAvatar);

			$sessionManager->flash('mensajeGlobal','su avatar fue registardo correctamente');
			$sessionManager->flash('correcto',true);

			return redirect('/');


		}
		return View('usuario/cambiarAvatar');
		$tUsuario=tUsuario::find($sessionManager->get('codigoUIsuario'));


	}
			public function actionIndexUsuario()
		{
			return view('usuario/indexsistema');
		}
}
?>