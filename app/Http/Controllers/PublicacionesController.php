<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
use Validator;
use App\Model\TUsuario;
use App\Model\TPublicacion;
use DB;


class PublicacionesController extends Controller
{
	public function actionIndex()
	{
	 $tPublicaciones=TPublicacion::all();

	 return view('publicacion/index',['tpublicacion'=>$tPublicaciones]);
	}
	public function actionInsert(Request $request,SessionManager $sesionManager)
	{
		if($_POST)
		{
		try
			{
				DB::beginTransaction();

				$tPublicacion=new TPublicacion();
 				$codigoPublicacion=uniqid();

 			
				$fileGetClientOriginalExtension=strtolower($request->file('fileExtencion')->getClientOriginalExtension());
				$rutaAvatarActual=public_path().'/imagenes/publicaciones/'.$codigoPublicacion.'.'.$fileGetClientOriginalExtension;
			
				if(file_exists($rutaAvatarActual))
				{
					unlink($rutaAvatarActual);
				}
				$request->file('fileExtencion')->move(public_path().'/imagenes/publicaciones/',$codigoPublicacion.'.'.$fileGetClientOriginalExtension);


				$tPublicacion->codigopublicacion=$codigoPublicacion;
				$tPublicacion->codigousuario='5aff4eb9edb54';//$sesionManager->get('codigousuario');
				$tPublicacion->titulo=$request->input('txtTitulo');
				$tPublicacion->descripcion=$request->input('txtDescripcion');
				$tPublicacion->cuerpo=$request->input('txtCuerpo');
				$tPublicacion->link=$request->input('txtLink');
				$tPublicacion->categoria='szdczx';//$request->input('txtCategoria');
				$tPublicacion->estado=$request->estado='activo';

				$tPublicacion->extensionPortada=$fileGetClientOriginalExtension;
			


			$tPublicacion->save();
			DB::commit();
			} 
			catch (Exception $e)
			{
				
			}
			

			return redirect('publicacion/insert');

		}

		return view('publicacion/insert');
	}
	public function actionTodas()
	{
		
		$tPublicaciones=TPublicacion::orderby('fecharegistro','DESC')->paginate(10);

			//dd($tPublicaciones);exit;
		return view('publicacion/todas',['tPublicacion'=>$tPublicaciones]);
	}

	public function actionVermas($id)
	{

		$tpublicacion=TPublicacion::find($id);

		return view('publicacion/vermas',['publicacion'=>$tpublicacion]);
	}


}
