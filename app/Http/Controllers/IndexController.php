<?php 
namespace App\Http\Controllers;
use Iluminate\Request;
use App\Model\TPublicacion;
use App\Model\TDirectorio;
use App\Model\TSlider;
use App\Model\TAnuncio;

	class IndexController extends Controller
	{	
		public function actionIndex()
		{			
			$tslider=TSlider::where('estado',true)->get();

			$tPublicacion=TPublicacion::orderby('fecharegistro','DESC')->take(10)->get();
			$tAnuncioimportante=TAnuncio::where('importante',true)->get();
			$tAnuncios=TAnuncio::whereRaw('estado=? and importante=?',[true,false])->orderby('fecharegistro','DESC')->take(10)->get();

			//dd($tAnuncioimportante);exit;


			return view('index/index',['tpublicaciones'=>$tPublicacion,'tslider'=>$tslider,'tanuncioImportante'=>$tAnuncioimportante,'tAnuncios'=>$tAnuncios]);
		}
		public function actionBienvenida()
		{
			return view('index/bienvenida');
		}
		
		public function actionDirectorio()
		{
			$tdirectorio=TDirectorio::all();
			return view('index/directorio',['tdirectorio'=>$tdirectorio]);
		}

		public function actionCarusel()
		{

		}
		
	}
 ?>