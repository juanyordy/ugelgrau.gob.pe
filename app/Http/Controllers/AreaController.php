<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
use Validator;
use DB;

use App\Model\TArea;

class AreaController extends Controller
{
	public function actionIndex()
	{
		$areas=TArea::all();
		return view('area/index',['areas'=>$areas]);
	}

	public function actionEdit(Request $request,SessionManager $sessionManager)
	{
		if($request->has('hdi'))
		{
			$validator=Validator::make(
				[
					'Nombre' => trim($request->input('txtNombre')),
				],
				[
					'Nombre' => ['required'],
					'Nombre' => ['unique:tarea,nombre,'.$sessionManager->get('TxtCodigoArea').',codigoarea']
				],
				[
					'unique' => 'El ":Attribute" ya se encuentra registrado en el sistema.<br>',
				]
				);
			$mensajeGlobal='';

			if($validator->fails())
			{
				$errors=$validator->errors()->all();

				foreach($errors as $value)
				{
					$mensajeGlobal.=$value;
				}
			}

			if($mensajeGlobal!='')
			{
				$sessionManager->flash('mensajeGlobal', $mensajeGlobal);
				$sessionManager->flash('correcto', false);

				return redirect('area/index');
			}

			$tArea=TArea::find($request->input('txtCodigoArea'));
			$tArea->nombre=$request->input('txtNombre');

			$tArea->save();

			return redirect('area/index');
		}

		$tarea=TArea::find($request->input('codigoarea'));

		return view('area/editar',['tarea'=>$tarea]);
	}
	public function actionToblock(Request $request)
	{
		if($request->has('hdb'))
		{
			$tArea=TArea::find($request->input('txtCodigoArea'));


			$tArea->activo=$tArea->activo?false:true;

			$tArea->save();
			return redirect('area/index');
		}

		$tarea=TArea::find($request->input('codigoArea'));

		return view('area/bloquear',['tarea'=>$tarea]);
	}

	public function actionInsert(Request $request,SessionManager $sessionManager)
	{
		if($_POST)
		{
			$mensajeGlobal='';
			try
			{
				DB::beginTransaction();
				$tAreavalidar=TArea::where('nombre',$request->input('txtnombre'))->first();

				if($tAreavalidar!=null)
				{
					$sessionManager->flash('mensajeGlobal','La oficina   '.$request->input('txtnombre').' ya se encuenta registrado');
					$sessionManager->flash('correcto',false);

					return redirect('area/index');
				}

				$tArea=new TArea();


				$tArea->codigoArea=uniqid();
				$tArea->nombre=$request->input('txtnombre');
				$tArea->descripcion=$request->input('txtdescripcion');
				$tArea->activo=true;
				$tArea->save();

				DB::commit();
				$mensajeGlobal='Datos Guardos correctamente';

				$sessionManager->flash('mensajeGlobal', $mensajeGlobal);
				$sessionManager->flash('correcto',true);

			  return redirect('area/index');


			}
			catch (Exception $e)
			{

			}
		}

		return view('area/insert');
	}

}
?>
