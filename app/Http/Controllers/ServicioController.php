<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use App\Model\TServicio;

class ServicioController extends Controller
{
	public function actionIndex()
	{
		$listaServicios=TServicio::All();

		return view('servicio/index',['listaServicios'=>$listaServicios]);
	
	}
	public function actionInsert(Request $request,SessionManager $sesionMananger)
	{
		if($_POST)
		{	
			$tServicio=new TServicio();

			$tServicio->codigoServicio=uniqid();
			$tServicio->codigoUsuario=$sesionMananger->get('codigoUsuario');
			$tServicio->titulo=$request->input('txtTitulo');
			$tServicio->descripcion=$request->input('txtDescripcion');
			$tServicio->contenido=$request->input('txtContenido');

			$tServicio->save();
			
			$sesionMananger->flash('mensajeGlobal','El servicio se registro correctamente');
			$sesionMananger->flash('correcto',true);

			return view('servicio/insert');

		}
		return view('servicio/insert');
	}
	Public function actionEdit(Request $request,SessionManager $sessionManager)
	{
		if($_POST)
		{
			if($request->has('hdEdit'))
			{
				$tServicioEditar=tServicio::find($request->input('textCodigoServicio'));

				//dd($tServicioEditar);exit();
				$tServicioEditar->titulo=$request->input('textTitulo');
				$tServicioEditar->descripcion=$request->input('textDescripcion');
				$tServicioEditar->contenido=$request->input('textContenido');

				$tServicioEditar->save();

				$sessionManager->flash('mensajeGlobal','Servicio Editado Correctamente');
				$sessionManager->flash('correcto',true);

				return redirect('servicio/index');
			}

			$servicio=TServicio::find($request->input('codigoServicio'));
			//dd($servicio);exit;
			return view('servicio/edit',['servicio'=>$servicio]);

		}	
	}
	public function actionEliminar(SessionManager $sessionManager,$codigoServicio)
	{
		$tServicio=TServicio::find($codigoServicio);
		$tServicio->delete();


		$sessionManager->flash('mensajeGlobal','El servicio se elimino correctamente');
		$sessionManager->flash('correcto',true);
		return redirect('servicio/index');
	}
}
 ?>
