<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
use Validator;
use App\Model\TUsuario;
Use App\Model\TArea;

use DB;

use App\Model\TDirectorio;

class DirectorioController extends Controller
{
	public function actionIndex()
	{
		$tdirectorio=TDirectorio::all();
	

		return view('directorio/index',['tdirectorio'=>$tdirectorio]);
	}

	
	public function actionInsert(Request $request,Encrypter $encrypter,SessionManager $sessionManager)
	{

		if ($request->has('htdirectorio'))
		{
			if(TUsuario::where('correoelectronico', trim($request->input('txtCorreoElectronico')))->count()>0)
			{
				$request->flash();
				

				$sessionManager->flash('mensajeGlobal', 'Usuario ya registrado anteriormente.');
				$sessionManager->flash('correcto', false);

				return redirect('directorio/insert');
			}
			if(TUsuario::where('dni', trim($request->input('txtdniRuc')))->count()>0)
			{
				$request->flash();
				
				$sessionManager->flash('mensajeGlobal', 'Usuario ya registrado anteriormente.');
				$sessionManager->flash('correcto', false);

				return redirect('directorio/insert');
			}
			

			//dd($request->input('selectCodigoArea'));exit;
			try 
			{
				 DB::beginTransaction();

				$tUsuario=new TUsuario();
				$codigoUsuarioTemp=uniqid();

				copy(public_path().'/avatar/user.png', public_path().'/avatar/'.$codigoUsuarioTemp.'.png');

				$tUsuario->codigousuario=$codigoUsuarioTemp;
				$tUsuario->dni=$request->input('txtdniRuc');
				$tUsuario->telefono=$request->input('txtCelular');
				$tUsuario->nombre=$request->input('txtNombre');
				$tUsuario->apellido=$request->input('txtApellido');
				$tUsuario->fechanacimiento='12/12/12';
				$tUsuario->direccion='Illarassa';
				$tUsuario->sexo=true;
				$tUsuario->correoelectronico=$request->input('txtCorreoElectronico');
				$tUsuario->contrasenia=$encrypter->encrypt('123');
				$tUsuario->extensionAvatar='png';
				
				$tUsuario->rol='profesor';
				$tUsuario->estado='activo';

				//dd($tUsuario);exit;
				$tUsuario->Save();

				$tdirectorio=new TDirectorio();

				$tdirectorio->codigodirectorio=uniqid();
				$tdirectorio->codigousuario=$codigoUsuarioTemp;
				$tdirectorio->nombre=$request->input('txtNombreArea');
				$tdirectorio->nombreapellido=$request->input('txtNombre').' '.$request->input('txtApellido');
				$tdirectorio->correoelectronico=$request->input('txtCorreoElectronico');

				$tdirectorio->save();

				

				 DB::commit();

				$sessionManager->flash('mensajeGlobal', 'registro correcto');
				$sessionManager->flash('correcto', true);

			//return redirect('directorio/index');
			return redirect('directorio/index');

			} 
			catch (Exception $e)
			{
				$sessionManager->flash('mensajeGlobal', 'ocurrio un error ');
				$sessionManager->flash('correcto', false);

				//return redirect('directorio/index');
				return redirect('directorio/index');
				
			}
		}
		

		return view('directorio/insert');	
		
	}
	
}
?>
