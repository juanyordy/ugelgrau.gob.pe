<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use App\Model\TSlider;
use DB;
	class SliderController extends Controller
	{	


		public function actionInsert(Request $request,SessionManager $sessionManager)
		{
			
			if($_POST)
			{
			
				try
				{
					DB::beginTransaction();

					$tslider=new TSlider();
	 				$codigoSlider=uniqid();

	 			
					$fileGetClientOriginalExtension=strtolower($request->file('fileExtencion')->getClientOriginalExtension());
					$rutaAvatarActual=public_path().'/imagenes/slider/'.$codigoSlider.'.'.$fileGetClientOriginalExtension;
				
					if(file_exists($rutaAvatarActual))
					{
						unlink($rutaAvatarActual);
					}
					$request->file('fileExtencion')->move(public_path().'/imagenes/slider/',$codigoSlider.'.'.$fileGetClientOriginalExtension);


					$tslider->codigoslider=$codigoSlider;
					$tslider->codigousuario=$sessionManager->get('codigoUsuario');
					$tslider->titulo=$request->input('txtTitulo');
					$tslider->descripcion=$request->input('txtDescripcion');
			
					$tslider->estado=true;

					$tslider->extensionslider=$fileGetClientOriginalExtension;
				


				$tslider->save();
				DB::commit();
				} 
				catch (Exception $e)
				{
					
				}
				

				return redirect('slider/insert');

			}

			return view('slider/insert');
		}

		public function actionIndex()
		{
			$tslider=TSlider::all();

			return view('slider/index',['tslider'=>$tslider]);
		}

		
	}
 ?>