<?php 
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class TValor extends Model
{
	protected $table='tvalor';
	protected $primaryKey='codigovalor';
	public $incrementing=null;
	public $timestamps=true;
	
		const CREATED_AT = 'fecharegistro';
    const UPDATED_AT = 'fechamodificacion';

	public function usuario()
	{
		return belongsTo('App\Model\TUsuario','codigousuario');
	}
}

?>