<?php 
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class TPublicacion extends Model
{
	protected $table='tpublicacion';
	protected $primaryKey='codigopublicacion';
	public $incrementing=null;
	public $timestamps=true;

	const CREATED_AT = 'fecharegistro';
    const UPDATED_AT = 'fechamodificacion';

    public function usuario()
    {
    	return $this->belongsTo('App\Model\TUsuario','codigousuario');
    }
    public function comentario()
   	{
   		return $this->hasMany('App\Model\TComentario','codigocomentario');
   	}
}
?>