<?php 
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class TArea extends Model
{
	protected $table='tarea';
	protected $primaryKey='codigoarea';
	public $incrementing=null;
	public $timestamps=true;

	const CREATED_AT = 'fecharegistro';
    const UPDATED_AT = 'fechamodificacion';

	public function tdirectotio()
	{
		return $this->hasMany('App\Model\TDirectorio','codigoarea');
	}
}

?>