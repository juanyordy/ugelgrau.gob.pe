<?php 
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class TAnuncio extends Model
{
	protected $table='tanuncio';
	protected $primaryKey='codigoanuncio';
	public $incrementing=null;
	public $timestamps=true;

	const CREATED_AT = 'fecharegistro';
    const UPDATED_AT = 'fechamodificacion';

	public function tusuario()
	{
		return $this->hasMany('App\Model\TUsuario','codigousuario');
	}
}

?>