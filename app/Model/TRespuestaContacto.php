<?php 
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class TRespuestaContacto extends Model
{
	protected $table='trespuestacontacto';
	protected $primaryKey='codigorespuestacontacto';
	public $incrementing=null;
	public $timestamps=true;

	public function usuario()
	{
		return belongsTo('App\Model\TUsuario','codigousuario');
	}
	public function contacto()
	{
		return belongsTo('App\Model\TContacto','codigocontacto');
	}
}

?>