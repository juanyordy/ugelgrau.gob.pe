<?php 
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class TSlider extends Model
{
	protected $table='tslider';
	protected $primaryKey='codigoslider';
	public $incrementing=null;
	public $timestamps=true;

	const CREATED_AT = 'fecharegistro';
    const UPDATED_AT = 'fechamodificacion';
    

	public function usuario()
	{
		return belongsTo('App\Model\TUsuario','codigousuario');
	}
}

?>