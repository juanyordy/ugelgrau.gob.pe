<?php 
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class TComentario extends Model
{
	protected $table='tcomentario';
	protected $primaryKey='codigocomentario';
	public $incrementing=null;
	public $timestamps=true;

	const CREATED_AT = 'fecharegistro';
    const UPDATED_AT = 'fechamodificacion';

	public function usuario()
	{
		return belongsTo('App\Model\TPublicacion','codigopublicacion');
	}
}

?>