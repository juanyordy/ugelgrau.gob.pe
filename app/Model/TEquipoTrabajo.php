<?php 
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class TEquipoTrabajo extends Model
{
	protected $table='tequipotrabajo';
	protected $primaryKey='codigoequipotrabajo';
	public $incrementing=null;
	public $timestamps=true;

		const CREATED_AT = 'fecharegistro';
    const UPDATED_AT = 'fechamodificacion';

	public function usuario()
	{
		return belongsTo('App\Model\TUsuario','codigousuario');
	}
}

?>