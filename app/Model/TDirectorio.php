<?php 
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class TDirectorio extends Model
{
	protected $table='tdirectorio';
	protected $primaryKey='codigodirectorio';
	public $incrementing=null;
	public $timestamps=true;

	const CREATED_AT = 'fecharegistro';
    const UPDATED_AT = 'fechamodificacion';

	public function tarea()
	{
		return belongsTo('App\Model\TArea','codigoarea');
	}
}

?>