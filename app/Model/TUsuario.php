<?php 
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class TUsuario extends Model
{
	protected $table='tusuario';
	protected $primaryKey='codigousuario';
	public $incrementing=null;
	public $timestamps=true;

	const CREATED_AT = 'fecharegistro';
    const UPDATED_AT = 'fechamodificacion';

    public function servicio()
    {
    	return $this->hasMany('App\Model\TServicio','codigousuario');
    }
    public function equipotrabajo()
    {
    	return $this->hasMany('App\Model\TEquipoTrabajo','codigousuario');
    }
    public function publicacion()
    {
    	return $this->hasMany('App\Model\TPublicacion','codigousuario');
    }
    public function imageninstitucional()
    {
    	return $this->hasMany('App\Model\TImagenInstitucional','codigousuario');
    }
    public function valor()
    {
    	return $this->hasMany('App\Model\TValor','codigousuario');
    }
    public function respuestacontacto()

    {
    	return $this->hasMany('App\Model\TRespuestaContacto','codigousuario');
    }
    public function banner()

    {
    	return $this->hasMany('App\Model\TBanner','codigousuario');
    }

    public function tbienvenida()

    {
        return $this->hasMany('App\Model\TBienvenida','codigousuario');
    }

    public function tslider()

    {
        return $this->hasMany('App\Model\TSlider','codigousuario');
    }

    public function tanuncio()
    {
        return $this->hasMany('App\Model\TAnuncio','codigousuario');
    }
    
    

}

 ?>
