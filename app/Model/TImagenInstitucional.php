<?php 
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class TImagenInstitucional extends Model
{
	protected $table='timageninstitucional';
	protected $primaryKey='codigoimagen';
	public $incrementing=null;
	public $timestamps=true;

	const CREATED_AT = 'fecharegistro';
    const UPDATED_AT = 'fechamodificacion';

    public function usuario()
    {
    	return $this->belongsTo('App\Model\TUsuario','codigousuario');
    }
    
}
?>