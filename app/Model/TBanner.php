<?php 
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class TBanner extends Model
{
	protected $table='tbanner';
	protected $primaryKey='codigobanner';
	public $incrementing=null;
	public $timestamps=true;

	const CREATED_AT = 'fecharegistro';
    const UPDATED_AT = 'fechamodificacion';

	public function usuario()
	{
		return belongsTo('App\Model\TUsuario','codigoUsuario');
	}
}

?>