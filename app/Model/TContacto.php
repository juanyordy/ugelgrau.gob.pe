<?php 
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class TContacto extends Model
{
	protected $table='tcontacto';
	protected $primaryKey='codigocontacto';
	public $incrementing=null;
	public $timestamps=true;

	const CREATED_AT = 'fecharegistro';
    const UPDATED_AT = 'fechamodificacion';

	public function respuestacontacto()
	{
		return hasMany('App\Model\TRespuestaContacto','codigocontacto');
	}

}

?>