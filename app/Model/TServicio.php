<?php 
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class TServicio extends Model
{
	protected $table='tservicio';
	protected $primaryKey='codigoservicio';
		public $incrementing=null;
	public $timestamps=true;

	const CREATED_AT = 'fecharegistro';
    const UPDATED_AT = 'fechamodificacion';
    

	public function usuario()
	{
		return belongsTo('App\Model\TUsuario','codigousuario');
	}
}

?>