<?php

Route::Match(['get','post'],'usuario/login','UsuarioController@actionLogin');
Route::get('usuario/logout', 'UsuarioController@actionLogOut');
Route::get('/','IndexController@actionIndex');

/*Usuario*/

Route::get('usuario/index','UsuarioController@actionIndex');
Route::match(['get','post'],'usuario/insert','UsuarioController@actionInsert');
Route::Match(['get','post'],'usuario/editar','UsuarioController@actionEdit');
Route::Match(['get','post'],'usuario/cambiaravatar','UsuarioController@actionCambiarAvatar');
Route::get('/index','UsuarioController@actionIndexUsuario');
// action Servicios 
Route::get('servicio/index','ServicioController@actionIndex');
Route::Match(['get','post'],'servicio/insert','ServicioController@actionInsert');
Route::Match(['get','post'],'servicio/edit','ServicioController@actionEdit');
Route::get('servicio/eliminar/{codigoServicio}','ServicioController@actionEliminar');

/* publicaciones*/
Route::get('publicacion/index','PublicacionesController@actionIndex');
Route::match(['get','post'],'publicacion/insert','PublicacionesController@actionInsert');

Route::get('publicacion/vermas/{codigopublicacion}','PublicacionesController@actionVermas');

Route::get('publicacion/todas','PublicacionesController@actionTodas');
//Route::get('/','IndexController@actionIndex');
Route::get('bienvenida','BienvenidaController@actionBienvenida');
Route::match(['get','post'],'area/insert','AreaController@actionInsert');
Route::get('area/index','AreaController@actionIndex');
Route::post('area/edit','AreaController@actionEdit');
Route::post('area/toblock','AreaController@actionToblock');

/*Directorio*/
Route::get('directorio','IndexController@actionDirectorio');
Route::get('directorio/index','DirectorioController@actionIndex');


Route::match(['get','post'],'directorio/insert','DirectorioController@actionInsert');
//Route::post('directorio/insertM','DirectorioController@actionInsertM');

/*Slider*/

Route::match(['post','get'],'slider/insert','SliderController@actionInsert');
Route::get('slider/index','SliderController@actionIndex');

/*Anuncio*/
Route::match(['post','get'],'anuncio/insert','AnuncioController@actionInsert');
Route::get('anuncio/index','AnuncioController@actionIndex');
Route::post('anuncio/edit','AnuncioController@actionEdit');

