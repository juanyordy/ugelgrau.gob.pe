﻿'use strict';

var AjaxHelper = {
    lockScreen: function () {
        var lock = $('<div class="box" id="lock_screen">');

        lock.css({
            display: 'block',
            position: 'fixed',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            background: 'rgba(0,0,0,0.3)',
            'z-index': '9999'
        });

        var overlay = $('<div class="overlay">');
        var spinLoading = $('<i style="font-size:80px;color:#2919C3" class="fa fa-spinner fa-spin">');

        overlay.append(spinLoading);
        lock.append(overlay);

        $('body').append(lock);        

    },
    unLockScreen: function () {
        $('#lock_screen').remove();
    },
    showError: function () {

        $('#lock_screen').remove();

        swal(
            'Error',
            'Ocurrio un error, intentelo más tarde o contactese con el administrador.',
            'error'
        );
    }
};