@extends('template.templategeneric')
@section('tituloGeneral','Servicio')
@section('subTitulo','Lista de servicios')
@section('cuerpoGeneral')
<section class="content">

 <div class="row">
  @foreach($listaServicios as $servicios)
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>150</h3>

              <p>{{ $servicios->titulo }}</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" onclick="editarajax('{{$servicios->codigoservicio}}');" class="small-box-footer">Editar<i class="fa fa-arrow-circle-right"></i></a>
              <a href="#" onclick="eliminarServicio('{{$servicios->codigoservicio}}');" class="small-box-footer bg-danger">Eliminar<i class="fa fa-arrow-circle-right " ></i></a>
          </div>
        </div>
        @endforeach
       
        <!-- ./col -->
      </div>
      <div id="modal"></div>
</section>
<script>
  function editarajax(codigoServicio)
  {
   $.ajax(
   {
    url:'{{ url('servicio/edit') }}',
    type:'Post',
    data:{ _token :'{{csrf_token()}}',codigoServicio:codigoServicio},
    cache: false,
    async: true

   }).done(function(resultado)
   {
    $('#modal').html(resultado);
    $('#modal-primary').modal('show')
     


   }).fail(function()
   {
    swal('Error', 'Error no controlado.', 'error');
   });

  }
  function eliminarServicio(codigoServicio)
  {
     swal({
                title: '¿Estas Seguro?',
                text: "Eliminar.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#39843A',
                cancelButtonColor: '#dd4b39',
                confirmButtonText: 'Eliminar servicio'
            }).then(function (confirm) {
                
                window.location.href='{{ url('servicio/eliminar')}}/'+codigoServicio;
            });
  }
</script>
@endsection