<div class="modal modal-primary fade" id="modal-primary">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Ediat Servicio</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="frmEditarServicio" action="{{ url('servicio/edit') }}" method="post">
                       <div class="col-md-12">
                          <div class="form-group">
                            <input type="text" id="titulo" name="textTitulo" class="form-control" value="{{ $servicio->titulo }}">
                        </div>
                       </div> 
                        <div class="col-md-6">
                          <div class="form-group">
                            <textarea name="textDescripcion" id="descripcion">
                                {{ $servicio->descripcion }}
                            </textarea>
                            
                          </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                              <textarea name="textContenido" id="contenido">
                                  {{ $servicio->contenido }}
                              </textarea>
                            
                            </div>
                            <input type="hidden" name="textCodigoServicio" value="{{ $servicio->codigoservicio }}">
                            <input type="hidden" name="hdEdit">
                            {{ csrf_field() }}
                        </div>   
                    </form>
                </div>    
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
               <button type="button" class="btn btn-primary" onclick="enviarFormulario(event)">Guardar Cambios</button>
            </div>
        </div>
    </div>
</div>


<script>
    $(function () {
        CKEDITOR.replace('descripcion');
        CKEDITOR.replace('contenido');
    });

    $('#frmEditarServicio').formValidation(
    {
        framework: 'bootstrap',
        excluded: [':disabled', ':hidden', ':not(:visible)', '[class*="notValidate"]'],
        live: 'enabled',
        message: '<b style="color: #9d9d9d;">Asegúrese que realmente no necesita este valor.</b>',
        trigger: null,
        fields:
        {
            titulo:
            {
                validators:
                {
                    notEmpty:
                    {
                        message: '<b style="color: red;">El campo "Asunto" es requerido.</b>'
                    }
                }
            }

        }
    });

    function enviarFormulario(e)
    {
        e.preventDefault();
        $('#frmEditarServicio').data('formValidation').validate();
        if ($('#frmEditarServicio').data('formValidation').isValid()) {
            swal({
                title: '¿Estas Seguro?',
                text: "Enviar correo a todos.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#39843A',
                cancelButtonColor: '#dd4b39',
                confirmButtonText: 'Si, Enviar.'
            }).then(function (confirm) {
                
                $('#frmEditarServicio')[0].submit();
            });
        }
    }

</script>