@extends('template.templategeneric')
@section('tituloGeneral', 'Oficina')
@section('subTituloGeneral', 'Lista')
@section('cuerpoGeneral')

<div class="col-md-12">
<div class="box box-primary">
  <div class="box-header">
      <div class="row">
          <div class="col-md-9">
              <h3 class="box-title">Lista de oficinas</h3>
          </div>
          <div class="col-md-3">
              <div class="form-group">
                  <div class="box-tools">
                      <div class="input-group input-group-sm" style="width: 150px;">
                          <div class="input-group-btn">
                            <a href="{{url('area/insert')}}" class="btn btn-block btn-success btn-flat">Nuevo</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

    <div class="box-body table-responsive no-padding">
        <table id="example2" class="table table-hover">
            <thead>
                <tr>
                <th>Nombre</th>
                <th>Activo</th>
                <th>Fecha de Registro</th>
                <th>Acciones</th>
                </tr>
            </thead>
             <tbody>

        @foreach ($areas as $area)

            <tr>
            <td>{{ $area->nombre }}</td>
            <td>
            	<a href="#" class="{{ $area->activo?'label label-success':'label label-warning'}}">{{ $area->activo?'Activo':'Inactivo' }}</a>
            </td>
            <td>
            	{{  $area->fechaRegistro}}</td>
            <td>
            	<a data-parameter="{{ $area->codigoarea }}" class="label label-info btn-editar">Editar</a>

            	<a data-parameter="{{$area->codigoarea }}" class="label {{$area->activo?'label-danger':'label-warning'}} btn-bloquear">{{ $area->activo?'Bloquear':'Activar' }}</a>
            </td>
             </tr>
        @endforeach
        </tbody>
        </table>
    </div>
</div>
</div>
<script src="{{ asset('js/AjaxHelper.js') }}"></script>

<script>
    $('.btn-editar').on('click',function(e)
    {
         AjaxHelper.lockScreen();
        $.ajax(
        {
            type:'post',
            dataType:'html',
            data:
                {
                'codigoarea': $(this).data('parameter'),_token : '{{csrf_token()}}'
                },
            url:"{{ url('area/edit') }}",
           success: function (json) {
                AjaxHelper.unLockScreen();

                if (json) {
                    $('#contenedorGeneral').html(json);
                    $('#modal-editar').modal('show');
                }
                else {
                     AjaxHelper.showError();
                }
            },
            error:AjaxHelper.showError

        });
    });
    $('.btn-bloquear').on('click',function()
    {
        AjaxHelper.lockScreen();
        $.ajax(
        {
        type:'post',
            dataType:'html',
            data:
                {
              _token : '{{csrf_token()}}', 'codigoArea': $(this).data('parameter')
                },
             url:"{{ url('area/toblock') }}",
            success: function (json) {
                AjaxHelper.unLockScreen();

                if (json) {
                    $('#contenedorGeneral').html(json);
                    $('#modal-bloquear').modal('show');
                }
                else {
                     AjaxHelper.showError();
                }
            },
            error:AjaxHelper.showError

        });
    });
    $(function () {
      $('#example1').DataTable()
      $('#example2').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      })
    })
</script>
@endsection
