<div class="modal fade" id="modal-editar">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Editar</h4>
            </div>
            <form id="frmderivar" action="{{ url('area/edit') }}" method="post">
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                        <div class="col-md-12">
                            <div class="col group">
                                {{ csrf_field() }}

                                <input type="hidden" name="txtCodigoArea" value="{{ $tarea->codigoarea }}">
                                <input type="hidden" name="hdi">
                                     <label>{{$tarea->nombre}}</label> 
                            </div>
    
                            <div class="col group">
                                <input type="tex" name="txtNombre" class="form-control">
                            </div>
                        </div>
                       
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="enviarFormulario(event)">Guardar cambios</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
   

    $('#frmderivar').formValidation(
	{
		framework: 'bootstrap',
		excluded: [':disabled', ':hidden', ':not(:visible)', '[class*="notValidate"]'],
		live: 'enabled',
		message: '<b style="color: #9d9d9d;">Asegúrese que realmente no necesita este valor.</b>',
		trigger: null,
		fields:
		{
			txtNombre:
			{
				validators:
				{
					notEmpty:
					{
						message: '<b style="color: red;">El campo "Nombre" es requerido.</b>'
					}
				}
			}
		}
	});

    $(function()
    {

    });

  
    function enviarFormulario(e)
    {
        e.preventDefault();

        resetFieldsfrmderivar();

        $('#frmderivar').data('formValidation').validate();

        if ($('#frmderivar').data('formValidation').isValid()) {
            swal({
                title: '¿Estas Seguro?',
                text: "Cambiaras los datos de la oficina .",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#39843A',
                cancelButtonColor: '#dd4b39',
                confirmButtonText: 'Si, Modifiar.'
            }).then(function (confirm) {
                $('#frmderivar')[0].submit();
            });
        }
    }

    function resetFieldsfrmderivar()
    {
        $('#frmderivar').data('formValidation').resetField($('#nombre'));
    }