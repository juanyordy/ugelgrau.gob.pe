<div class="modal fade" id="modal-bloquear">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color:{{$tarea->estado?'#F80C0C':'#268C9E' }}">
                <button type="button" class="close btn-primary" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" style="font-family: cursive;"> {{$tarea->estado? 'Bloquear':'Activar' }}</h4>
            </div>
            <form id="frmderivar" action="{{ url('area/bloquear') }}" method="post">
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <p>{{ $tarea->nombre }}</p>
                           
                       <input type="hidden" name="hdb">
                       <input type="hidden" name="txtCodigoArea" value="{{$tarea->codigoArea}}">
                       {{csrf_field() }}
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="enviarFormulario(event)">Guardar cambios</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function enviarFormulario(e)
    {       
        swal({
            title: '¿Estas Seguro de ?',
            text: "Cambiar  estado de area.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#39843A',
            cancelButtonColor: '#dd4b39',
            confirmButtonText: 'Si, Modifiar.'
        }).then(function (confirm) {
            $('#frmderivar')[0].submit();
        });
     
    }

</script>