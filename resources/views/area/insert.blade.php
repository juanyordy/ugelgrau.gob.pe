@extends('template.templategeneric')
@section('tituloGeneral', 'Oficina')
@section('subTituloGeneral', 'Registrar')
@section('cuerpoGeneral')
<div class="col-md-12">
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Formulario de registro de  Oficina</h3>
    </div>
    <form id="frmRegistar" action="{{ url('area/insert') }}" method="post">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="col-md-12">
                             <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" id="nombre" name="txtnombre" class="form-control" placeholder="Nombre" value="">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nombre">Descripcion</label>
                            <textarea name="txtdescripcion" id="descripcion" class="form-control" placeholder="Descipcion"></textarea>
                            {{ csrf_field() }}
                        </div>
                    </div>
                        </div>
                        
                </div>  
                
            </div>
        </div>
        <div class="box-footer">

            <input type="button" class="btn btn-primary" id="registroUsuario" onclick="enviarFormulario(event)" name="registroUsuario" value="Registrar">
        </div>
    </form>
</div>
</div>


<script>

    $(function () {

        $('#frmRegistar').formValidation(
		{
		    framework: 'bootstrap',
		    excluded: [':disabled', ':hidden', ':not(:visible)', '[class*="notValidate"]'],
		    live: 'enabled',
		    message: '<b style="color: #9d9d9d;">Asegúrese que realmente no necesita este valor.</b>',
		    trigger: null,
		    fields:
			{
			    codigoInstitucionEducativa:
                {
                    validators:
                    {
                        notEmpty:
						{
						    message: '<b style="color: red;">El campo "Institución educativa" es requerido.</b>'
						}
                    }
                },
			    nombre:
				{
				    validators:
					{
					    notEmpty:
						{
						    message: '<b style="color: red;">El campo "Nombre" es requerido.</b>'
						}
					}
				}
			}
		});
    });

    function enviarFormulario(e) {
        e.preventDefault();

        resetFieldsFrmEditar();

        $('#frmRegistar').data('formValidation').validate();

        if ($('#frmRegistar').data('formValidation').isValid()) {
            swal({
                title: '¿Estas Seguro?',
                text: "Guardar datos de la oficina.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#39843A',
                cancelButtonColor: '#dd4b39',
                confirmButtonText: 'Si, Guardar.'
            }).then(function (confirm) {
                $('#frmRegistar')[0].submit();
            });
        }
    }

    function resetFieldsFrmEditar()
    {
        $('#frmRegistar').data('formValidation').resetField($('#nombre'));
     
    }
</script>
@endsection