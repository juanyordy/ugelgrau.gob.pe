@extends('template.templateprincipal')
@section('cuerpoGeneral')

<section class="pages pages-ugel">
	<article class="container main-content">
	<div class="header_top">
	<div class="container">
		<header>
			<h1 class="title">UgeL Grau</h1>
			
		</header>
		<div class="breadcrumb">
			<a href="#">Inicio</a> &gt;
				<a href="#">UGEL GRAU</a> &gt;
			
			
				<span class="sombra_bread">Bienvenida</span>
		</div>
	</div>
	</div>		   
	<div class="row">
		<div class="col-md-3">
		<ul class="menu-lateral">
			<li class="item current"><a href="https://www.ugel01.gob.pe/ugel/bienvenida/" title="Bienvenida">Bienvenida</a></li>
				<li class="item "><a href="https://www.ugel01.gob.pe/ugel/organigrama/" title="Organigrama">Organigrama</a></li>
				<li class="item "><a href="https://www.ugel01.gob.pe/ugel/jurisdiccion/" title="Jurisdicción">Jurisdicción</a></li>
				<li class="item "><a href="https://www.ugel01.gob.pe/ugel/presentacion/" title="Presentación">Presentación</a></li>
		</ul>
		</div>
		<div class="col-md-9">
		<h2 class="main_title">Bienvenida</h2>
		<div class="post-content">
			
	<div style="width: 640px; " class="wp-video"><!--[if lt IE 9]><script>document.createElement('video');</script><![endif]-->
<span class="mejs-offscreen">Video Player</span>
<div id="mep_0" class="mejs-container svg wp-video-shortcode mejs-video" tabindex="0" role="application" aria-label="Video Player" style="width: 640px; height: 360px;"><div class="mejs-inner"><div class="mejs-mediaelement">
	<video class="wp-video-shortcode" id="video-4494-1" width="640" height="360" autoplay="1" preload="metadata" src="http://www.ugel01.gob.pe/wp-content/uploads/2016/06/Saludo-Director-Luis-Yataco-Ramirez-2018.mp4?_=1" style="width: 100%; height: 100%;">
	<source type="video/mp4" src="http://www.ugel01.gob.pe/wp-content/uploads/2016/06/Saludo-Director-Luis-Yataco-Ramirez-2018.mp4?_=1"><a href="http://www.ugel01.gob.pe/wp-content/uploads/2016/06/Saludo-Director-Luis-Yataco-Ramirez-2018.mp4">http://www.ugel01.gob.pe/wp-content/uploads/2016/06/Saludo-Director-Luis-Yataco-Ramirez-2018.mp4</a></video></div><div class="mejs-layers"><div class="mejs-poster mejs-layer" style="display: none; width: 100%; height: 100%;"></div><div class="mejs-overlay mejs-layer" style="width: 100%; height: 100%; display: none;"><div class="mejs-overlay-loading"><span></span></div></div><div class="mejs-overlay mejs-layer" style="display: none; width: 100%; height: 100%;"><div class="mejs-overlay-error"></div></div><div class="mejs-overlay mejs-layer mejs-overlay-play" style="width: 100%; height: 100%;"><div class="mejs-overlay-button"></div></div></div><div class="mejs-controls" style="display: block; visibility: hidden;"><div class="mejs-button mejs-playpause-button mejs-play"><button type="button" aria-controls="mep_0" title="Play" aria-label="Play"></button></div><div class="mejs-time mejs-currenttime-container" role="timer" aria-live="off"><span class="mejs-currenttime">00:00</span></div><div class="mejs-time-rail" style="width: 494px;"><span class="mejs-time-total mejs-time-slider" aria-label="Time Slider" aria-valuemin="0" aria-valuemax="NaN" aria-valuenow="0" aria-valuetext="00:00" role="slider" tabindex="0" style="width: 484px;"><span class="mejs-time-buffering" style="display: none;"></span><span class="mejs-time-loaded" style="width: 17.9847px;"></span><span class="mejs-time-current" style="width: 0px;"></span><span class="mejs-time-handle" style="left: -5px;"></span><span class="mejs-time-float"><span class="mejs-time-float-current">00:00</span><span class="mejs-time-float-corner"></span></span></span></div><div class="mejs-time mejs-duration-container"><span class="mejs-duration">01:07</span></div><div class="mejs-button mejs-volume-button mejs-mute"><button type="button" aria-controls="mep_0" title="Silenciar" aria-label="Silenciar"></button><a href="javascript:void(0);" class="mejs-volume-slider" style="display: none;"><span class="mejs-offscreen">Use Up/Down Arrow keys to increase or decrease volume.</span><div class="mejs-volume-total"></div><div class="mejs-volume-current" style="height: 80px; top: 28px;"></div><div class="mejs-volume-handle" style="top: 25px;"></div></a></div><div class="mejs-button mejs-fullscreen-button"><button type="button" aria-controls="mep_0" title="Pantalla completa" aria-label="Pantalla completa"></button></div></div><div class="mejs-clear"></div></div>
	</div>
</div>
<p style="text-align: justify;">Bienvenidos al portal institucional de la UGEL 01, a través de este instrumento podrás resolver tus necesidades de información y orientación. En la Unidad de Gestión Educativa Local N° 01 estamos comprometido en lograr una educación de calidad para todos los estudiantes de Lima Sur; con maestros bien preparados que puedan ejercer profesionalmente la docencia y sobre todo contar con instituciones educativas acogedoras que enseñen bien con profesionalismo y amor a nuestros niños y jóvenes para que logren el éxito.</p>
<p style="text-align: justify;">Nuestra jurisdicción territorial posee características heterogéneas en el aspecto demográfico, geográfico y económico y comprende los siguientes distritos: San Juan de Miraflores, Villa María del Triunfo, Villa El Salvador, Pachacamac, Lurín, Punta Hermosa, Punta Negra, San Bartolo, Santa María del Mar, Pucusana y Chilca, la zona que comprende la urbanización Papa León XIII.</p>
<p style="text-align: justify;">Asimismo, para brindarte una atención de calidad hemos acondicionado una Plataforma de Atención al Ciudadano que te brinda una atención personalizada de lunes a viernes de 8.30 de la mañana a 4.30 de la tarde. Además, puedes comunicarte con nosotros llamando por teléfono al 719-1890 o escribirnos al correo institucional informaciones@ugel01.gob.pe contamos además, con una ventanilla exclusiva para nuestros directores y coordinadores de Red que por sus funciones requieren una atención oportuna.</p>
<p style="text-align: justify;">¡Gracias por visitar nuestro portal web!</p>
<p><strong>Mg. Luis Alberto Yataco Ramírez</strong></p>
				    </div>
			    </div>
		    </div>
		</article>
    </section>
    @endsection