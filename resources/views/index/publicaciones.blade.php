
<style>
		        	
.recent-work-wrap
{
  position: relative;
}

.recent-work-wrap img{
  width: 115%;

}

.recent-work-wrap .recent-work-inner{
  top: 0;
  background: transparent;
  opacity: .8;
  width: 115%;
  border-radius: 0;
  margin-bottom: 0;
}

.recent-work-wrap .recent-work-inner h3{
  margin: 30px 0;
}

.recent-work-wrap .recent-work-inner h3 a{
  font-size: 24px;
  color: #fff;
}

.recent-work-wrap .overlay {
  position: absolute;
  top: 0;
  left: 0;
  width: 115%;
  height: 115%;
  opacity: 0;
  border-radius: 0;
  background: #c52d2f;
  color: #fff;
  vertical-align: middle;
  -webkit-transition: opacity 500ms;
  -moz-transition: opacity 500ms;
  -o-transition: opacity 500ms;
  transition: opacity 500ms;  
  padding: 30px;
}

.recent-work-wrap .overlay .preview {
  bottom: 0;
  display: inline-block;
  height: 60px;
  line-height: 60px;
  border-radius: 0;
  background: transparent;
  text-align: center;
  color: #fff;
}

.recent-work-wrap:hover .overlay {
  opacity: 1;
}
.img-responsive,
.thumbnail > img,
.thumbnail a > img,
.carousel-inner > .item > img,
.carousel-inner > .item > a > img {
  display: block;
  max-width: 100%;
  height: auto;
}
.img-rounded {
  border-radius: 6px;
}
.img-thumbnail {
  display: inline-block;
  max-width: 100%;
  height: auto;
  padding: 4px;
  line-height: 1.42857143;
  background-color: #fff;
  border: 1px solid #ddd;
  border-radius: 4px;
  -webkit-transition: all .2s ease-in-out;
       -o-transition: all .2s ease-in-out;
          transition: all .2s ease-in-out;
}
.img-circle {
  border-radius: 50%;
}
figure {
  margin: 1em 40px;
}
figure {
  margin: 0;
}
figure{
  display: block;
}

		        </style>
<div class="container homepage">
    <div class="row">
        <div class="col-md-9 ladoA">
	        <div class="noticias">
		        <div class="header_ugel">
			        <h3 class="page-titulo">PUBLICACIONES</h3>
			        <a href="{{url('publicacion/todas')}}" title="Todas las publicaciones" class="ver_todas">Ver Todas las publicaciones</a>
		        </div>

		        <ul class="lista_noticias">
                 @foreach($tpublicaciones as $publicacion)
              <li class="noticia-home">
              		<div class="col-md-6">
                      <div class="">
                        <a href="" title="{{$publicacion->titulo}}">
                        <figure class="figure">
                          <img src="{{asset('imagenes/publicaciones').'/'.$publicacion->codigopublicacion.'.'.$publicacion->extensionportada}}" class="img-thumbnail" alt="{{$publicacion->titulo}}">

                         </figure>

                      </div>
                    </div>
                      <div class="col-md-6">
                      <div class="nTitulo">
                        <a href="{{$publicacion->link}}" title="{{$publicacion->titulo}}">{{$publicacion->titulo}}</a></div>
                      <div class="fecha">{{$publicacion->fecharegistro}}</div>
                      <div class="dContent">
                          {!!$publicacion->descripcion!!}
                          <a href="{{url('publicacion/vermas').'/'.$publicacion->codigopublicacion}}" class="more-link"><span>Continuar Leyendo</span> <i class="mas"> </i></a>
                      </div>
                    </div>
                  </li>
                  @endforeach
				    </ul>
	       </div>
        </div>

        <div class="col-md-3 ladoB">
	        <aside>
		        <ul class="banners-list">
			        <li>
				        <h4 class="aTitulo">Anuncios Importantes</h4>
				        <div id="dCarruselAvisos" class="owl-carousel owl-theme owl-loaded">
					        						        							        
 					        
				        <div class="owl-stage-outer">

				        	<div class="owl-stage" style="transform: translate3d(-738px, 0px, 0px); transition: 0.45s; width: 2214px;">

				        		@foreach($tanuncioImportante as $anuncio)

					        	<div class="owl-item cloned" style="width: 246px; margin-right: 0px;">
                      <div class="item">
											<a href="#" title="{{$anuncio->titulo}}">
                        <img width="220" height="250" src="{{asset('imagenes/anuncio').'/'.$anuncio->codigoanuncio.'.'.$anuncio->extensionanuncio}}" class="img-thumbnail" alt="COAR 1 24-01-18">
											<button class="btn-enlace_externo">Ver más </button>
									        </a>
								        </div>
								</div>

				        		@endforeach
                    		</div>
              		  </div>


                	<div class="owl-controls">
                		<div class="owl-nav">
                			<div class="owl-prev" style="display: none;"></div>
                			<div class="owl-next" style="display: none;"></div>
                		</div>

                		<div class="owl-dots" style="display: none;">
                		
                		</div>
           			</div>

           			</div>
				        <!--<h4 class="aTitulo"><a href="#">Ver Todos</a></h4>-->
			        
			       </li>


			       @foreach($tAnuncios as $anuncio)
			       		<li class="banner-single">

							<a href="#" title="{{$anuncio->titulo}}">
								<img  class="img-responsive" src="{{asset('imagenes/anuncio').'/'.$anuncio->codigoanuncio.'.'.$anuncio->extensionanuncio}}" alt="{{$anuncio->titulo}}">
							</a>

						</li>
			       @endforeach

	        				        					        				        				        			        

		        </ul>
	        </aside>
        </div>
    </div>
	<div class="row">
		<div class="col-md-12 grupoenlaces">
	
	<div id="dCarruselGrupo1" class="owl-carousel owl-theme owl-loaded">

			<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-2400.67px, 0px, 0px); transition: 0.45s; width: 3878.01px;">
        <div class="owl-item cloned" style="width: 184.667px; margin-right: 0px;">
          <div class="item">

            <a href="http://sistemas06.minedu.gob.pe/sinadmed_1/resolucionesexternas/consultanormas.aspx" target="_blank" title="Sije">
             <img src="{{asset('ugel/sije.png')}}" alt="Sije">
            </a>
          </div>
      </div>
      <div class="owl-item cloned" style="width: 184.667px; margin-right: 0px;">
          <div class="item">
          <a href="http://www.pronabec.gob.pe/" target="_blank" title="Programa Beca 18">
          <img src="{{asset('ugel/beca18.png')}}" alt="Programa Beca 18">
        </a></div>
    </div>
    <div class="owl-item cloned" style="width: 184.667px; margin-right: 0px;">
        <div class="item">
        <a href="http://www.pronied.gob.pe/" target="_blank" title="Pronied">
        <img src="{{asset('ugel/pronied.png')}}" alt="Pronied">
    </a></div></div><div class="owl-item cloned" style="width: 184.667px; margin-right: 0px;">
        <div class="item">
        <a href="http://www.sineace.gob.pe/" target="_blank" title="Sineace">
        <img src="{{asset('ugel/sineace.png')}}" alt="Sineace">
    </a></div></div><div class="owl-item cloned" style="width: 184.667px; margin-right: 0px;">
        <div class="item">
        <a href="http://www.perueduca.pe/" target="_blank" title="Perú Educa">
        <img src="{{asset('ugel/peru_educa.png')}}" alt="Perú Educa">
    </a></div></div><div class="owl-item cloned" style="width: 184.667px; margin-right: 0px;">
        <div class="item">
        <a href="http://www.siseve.pe/" target="_blank" title="SiseVe">
        <img src="{{asset('ugel/siseve.png')}}" alt="SiseVe">
    </a></div></div><div class="owl-item" style="width: 184.667px; margin-right: 0px;">
        <div class="item">
        <a href="http://www.minedu.gob.pe/campanias/coe.php" target="_blank" title="COE MINEDU">
        <img src="{{asset('ugel/COE-MINEDU.jpg')}}" alt="COE MINEDU">
    </a></div></div><div class="owl-item" style="width: 184.667px; margin-right: 0px;">
        <div class="item">
        <a href="http://divertivoto.com/" target="_blank" title="Diverti Voto">
        <img src="{{asset('ugel/divertivotoonpe.jpg')}}" alt="Diverti Voto">
    </a></div></div><div class="owl-item" style="width: 184.667px; margin-right: 0px;">
        <div class="item">
        <a href="#" target="_blank" title="Tupa">
        <img src="{{asset('ugel/Tupa.jpg')}}" alt="Tupa">
    </a></div></div><div class="owl-item" style="width: 184.667px; margin-right: 0px;">
        <div class="item">
        <a href="http://sistemas06.minedu.gob.pe/sinadmed_1/resolucionesexternas/consultanormas.aspx" target="_blank" title="Sije">
        <img src="{{asset('ugel/sije.png')}}" alt="Sije">
    </a></div></div><div class="owl-item" style="width: 184.667px; margin-right: 0px;">
        <div class="item">
        <a href="http://www.pronabec.gob.pe/" target="_blank" title="Programa Beca 18">
        <img src="{{asset('ugel/beca18.png')}}" alt="Programa Beca 18">
    </a></div></div><div class="owl-item" style="width: 184.667px; margin-right: 0px;">
        <div class="item">
        <a href="http://www.pronied.gob.pe/" target="_blank" title="Pronied">
        <img src="{{asset('ugel/pronied.png')}}" alt="Pronied">
    </a></div></div><div class="owl-item" style="width: 184.667px; margin-right: 0px;">
        <div class="item">
        <a href="http://www.sineace.gob.pe/" target="_blank" title="Sineace">
        <img src="{{asset('ugel/sineace.png')}}" alt="Sineace">
    </a></div></div><div class="owl-item active" style="width: 184.667px; margin-right: 0px;">
        <div class="item">
        <a href="http://www.perueduca.pe/" target="_blank" title="Perú Educa">
        <img src="{{asset('ugel/peru_educa.png')}}" alt="Perú Educa">
    </a></div></div><div class="owl-item active" style="width: 184.667px; margin-right: 0px;">
        <div class="item">
        <a href="http://www.siseve.pe/" target="_blank" title="SiseVe">
        <img src="{{asset('ugel/siseve.png')}}" alt="SiseVe">
    </a></div></div><div class="owl-item cloned active" style="width: 184.667px; margin-right: 0px;">
        <div class="item">
        <a href="http://www.minedu.gob.pe/campanias/coe.php" target="_blank" title="COE MINEDU">
        <img src="{{asset('ugel/COE-MINEDU.jpg')}}" alt="COE MINEDU">
    </a></div></div><div class="owl-item cloned active" style="width: 184.667px; margin-right: 0px;">
        <div class="item">
        <a href="http://divertivoto.com/" target="_blank" title="Diverti Voto">
        <img src="{{asset('ugel/divertivotoonpe.jpg')}}" alt="Diverti Voto">
    </a></div></div><div class="owl-item cloned active" style="width: 184.667px; margin-right: 0px;">
        <div class="item">
        <a href="#" target="_blank" title="Tupa">
        <img src="{{asset('ugel/Tupa.jpg')}}" alt="Tupa">
    </a></div></div><div class="owl-item cloned active" style="width: 184.667px; margin-right: 0px;">
        <div class="item">
        <a href="http://sistemas06.minedu.gob.pe/sinadmed_1/resolucionesexternas/consultanormas.aspx" target="_blank" title="Sije">
        <img src="{{asset('ugel/sije.png')}}" alt="Sije">
    </a></div></div><div class="owl-item cloned" style="width: 184.667px; margin-right: 0px;">
        <div class="item">
        <a href="http://www.pronabec.gob.pe/" target="_blank" title="Programa Beca 18">
        <img src="{{asset('ugel/beca18.png')}}" alt="Programa Beca 18">
    </a></div></div><div class="owl-item cloned" style="width: 184.667px; margin-right: 0px;">
        <div class="item">
        <a href="http://www.pronied.gob.pe/" target="_blank" title="Pronied">
        <img src="{{asset('ugel/pronied.png')}}" alt="Pronied">
    </a></div></div></div></div><div class="owl-controls">
        <div class="owl-nav">
        <div class="owl-prev" style="">
      </div><div class="owl-next" style="">
      </div></div><div class="owl-dots" style="display: none;">
      </div></div>

  </div>
		</div>
	</div>

	<div class="row">

		<div class="col-md-12 grupoenlaces">

			<div id="dCarruselGrupo2" class="owl-carousel owl-theme owl-loaded">

			<div class="owl-stage-outer">
        <div class="owl-stage" style="transform: translate3d(-2236px, 0px, 0px); transition: 0.45s; width: 7080.67px;">
        <div class="owl-item cloned" style="width: 372.667px; margin-right: 0px;">
        <div class="item">
        <a href="http://escale.minedu.gob.pe/" target="_blank" title="Estadisticas de la calidad educacional">
        <img src="{{asset('ugel/escale.jpg')}}" alt="Estadisticas de la calidad educacional">

    </a></div></div><div class="owl-item cloned" style="width: 372.667px; margin-right: 0px;">

        <div class="item">

        <a href="http://escale2.minedu.gob.pe/estadistica/ce/" target="_blank" title="Escale Estadística On-line">

        <img src="{{asset('ugel/Estadistica-online-11-05-18.jpg')}}" alt="Escale Estadística On-line">

    </a></div></div><div class="owl-item cloned" style="width: 372.667px; margin-right: 0px;">

        <div class="item">

        <a href="http://www2.minedu.gob.pe/umc/noticias_index.php" target="_blank" title="Rutas del Aprendizaje">
          <img src="{{asset('ugel/rutas-del-aprendizaje.jpg')}}" alt="Rutas del Aprendizaje">
      </a></div></div><div class="owl-item" style="width: 372.667px; margin-right: 0px;">
          <div class="item">
          <a href="#" target="_blank" title="Resolución Directoral">
          <img src="{{asset('ugel/Resolucion-Directoral.jpg')}}" alt="Resolución Directoral">
      </a></div></div><div class="owl-item" style="width: 372.667px; margin-right: 0px;">
          <div class="item">
          <a href="http://www.sanciones.gob.pe:8081/transparencia/" target="_blank" title="RNSDD">
          <img src="{{asset('ugel/RNSDD.jpg')}}" alt="RNSDD"></a></div></div><div class="owl-item" style="width: 372.667px; margin-right: 0px;">

            <div class="item">

              <a href="#" target="_blank" title="Fiscaliza tu Cole">

              <img src="{{asset('ugel/Fiscaliza-tu-Cole.jpg')}}" alt="Fiscaliza tu Cole">

          </a></div></div><div class="owl-item active" style="width: 372.667px; margin-right: 0px;">

          <div class="item">

              <a href="#" target="_blank" title="Sistema Caja">

              <img src="{{asset('ugel/Sistema-Caja.jpg')}}" alt="Sistema Caja">

          </a></div></div><div class="owl-item active" style="width: 372.667px; margin-right: 0px;">

          <div class="item">

              <a href="#" target="_blank" title="Informe Esclafonario">

              <img src="{{asset('ugel/Copia-de-Informe-Escalafonario.jpg')}}" alt="Informe Esclafonario">

          </a></div></div><div class="owl-item active" style="width: 372.667px; margin-right: 0px;">

          <div class="item">

              <a href="http://identicole.minedu.gob.pe/inicio" target="_blank" title="Identicole">

              <img src="{{asset('ugel/identicole1.jpg')}}" alt="Identicole">

          </a></div></div><div class="owl-item" style="width: 372.667px; margin-right: 0px;">

          <div class="item">

              <a href="https://www.fonavi-st.gob.pe/sifonavi/" target="_blank" title="Secretria Técnica Fonavi">

              <img src="{{asset('ugel/secretaria_fonavi.jpg')}}" alt="Secretria Técnica Fonavi">

          </a></div></div><div class="owl-item" style="width: 372.667px; margin-right: 0px;">

          <div class="item">

              <a href="http://www.minedu.gob.pe/consulta/" target="_blank" title="Evaluación Censal Docentes Evaluados">

              <img src="{{asset('ugel/evaluacion_docente.jpg')}}" alt="Evaluación Censal Docentes Evaluados">

          </a></div></div><div class="owl-item" style="width: 372.667px; margin-right: 0px;">

          <div class="item">

              <a href="#" target="_blank" title="Consulta SINAD">

              <img src="{{asset('ugel/sinad.jpg')}}" alt="Consulta SINAD">

          </a></div></div><div class="owl-item" style="width: 372.667px; margin-right: 0px;">

          <div class="item">

              <a href="http://siagie.minedu.gob.pe/inicio/" target="_blank" title="Siagie">

              <img src="{{asset('ugel/siagie.jpg')}}" alt="Siagie">

          </a></div></div><div class="owl-item" style="width: 372.667px; margin-right: 0px;">

          <div class="item">

              <a href="http://escale.minedu.gob.pe/" target="_blank" title="Estadisticas de la calidad educacional">

              <img src="{{asset('ugel/escale.jpg')}}" alt="Estadisticas de la calidad educacional">

          </a></div></div><div class="owl-item" style="width: 372.667px; margin-right: 0px;">

          <div class="item">

              <a href="http://escale2.minedu.gob.pe/estadistica/ce/" target="_blank" title="Escale Estadística On-line">

              <img src="{{asset('ugel/Estadistica-online-11-05-18.jpg')}}" alt="Escale Estadística On-line">

          </a></div></div><div class="owl-item" style="width: 372.667px; margin-right: 0px;">

          <div class="item">

              <a href="http://www2.minedu.gob.pe/umc/noticias_index.php" target="_blank" title="Rutas del Aprendizaje">

              <img src="{{asset('ugel/rutas-del-aprendizaje.jpg')}}" alt="Rutas del Aprendizaje">

          </a></div></div><div class="owl-item cloned" style="width: 372.667px; margin-right: 0px;">

          <div class="item">

              <a href="#" target="_blank" title="Resolución Directoral">

              <img src="{{asset('ugel/Resolucion-Directoral.jpg')}}" alt="Resolución Directoral">

          </a></div></div><div class="owl-item cloned" style="width: 372.667px; margin-right: 0px;">

          <div class="item">

              <a href="#" target="_blank" title="RNSDD">

              <img src="{{asset('ugel/RNSDD.jpg')}}" alt="RNSDD">

          </a></div></div><div class="owl-item cloned" style="width: 372.667px; margin-right: 0px;">

          <div class="item">

              <a href="#" target="_blank" title="Fiscaliza tu Cole">

              <img src="{{asset('ugel/Fiscaliza-tu-Cole.jpg')}}" alt="Fiscaliza tu Cole">

          </a></div></div></div></div><div class="owl-controls">

          <div class="owl-nav">

              <div class="owl-prev" style="">

            </div><div class="owl-next" style="">

          </div></div><div class="owl-dots" style="display: none;"></div></div></div>
		</div>
	</div>


</div>
<script>
	

    		$("#dCarruselAvisos").owlCarousel ({
				items :1,
				dots:false,
				navText:["",""],
				nav : false,
				loop :true,
				autoplay :true,
				autoplayTimeout :1000,
				autoplayHoverPause :true,
				smartSpeed :450
			})

</script>