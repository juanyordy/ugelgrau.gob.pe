@extends('template.templateprincipal')
@section('cuerpoGeneral')
<section class="pages">
<article class="container main-content">
	<div class="header_top">
		<div class="container">
			<header>
				<h1 class="title">Directorio</h1>
				
			</header>
			<div class="breadcrumb">
				<a href="#">Inicio</a> &gt;		
				
				<span class="sombra_bread">Directorio</span>
			</div>
		</div>
	</div>
	<div class="row">
<div class="col-md-3">
	<ul class="menu-lateral">
			<li class="item "><a href="#" title="Contáctenos">Contáctenos</a></li>
			<li class="item current"><a href="#" title="Directorio">Directorio</a></li>
			<li class="item "><a href="#" title="Directorio Telefónico">Directorio Telefónico</a></li>
			<li class="item "><a href="#" title="Buzón de Sugerencia">Buzón de Sugerencia</a></li>
	
	</ul>
</div>
			    
			    <div class="col-md-9">
				    <div class="post-content">
					    					    <br>
					    <table class="main_table">
						    <tbody>
						    <tr>
							    <th>Área y/o Equipo</th>
							    <th>Nombres y Apellidos</th>
							    <th>Email</th>
						    </tr>
						    @foreach($tdirectorio as $directorio)
							    <tr>
								    <td class="negrita">{{$directorio->nombre}}</td>
								    <td>{{$directorio->nombreapellido}}</td>
								    <td><a href="mailto:lyatacor@ugel01.gob.pe" title="{{$directorio->nombreapellido}}">{{$directorio->correoelectronico}}</a></td>
								</tr>
						    @endforeach
							    
						    </tbody>
						</table>
				    </div>
			    </div>
		    </div>
	</article>
	</section>

    @endsection