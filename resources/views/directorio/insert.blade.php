<div class="modal fade" id="modal-insert">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Registrar</h4>
            </div>
             <form id="frmRegistar" action="{{ url('directorio/insert') }} "method="post">
             <div class="modal-body">

             <div class="box-body">
                <div class="form-group">
                    <label for="txtNombre">Area</label>
                    <input type="text" id="txtNombreArea" name="txtNombreArea" class="form-control" placeholder="Nombre" value="">

                </div>

                <div class="form-group">
                     <label for="nombre">Dni</label>
                     <input type="text" id="dni" name="txtdniRuc" class="form-control" placeholder="dni" value="">
                </div>

                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" id="txtNombre" name="txtNombre" class="form-control" placeholder="Nombre" value="">
                </div>

                <div class="form-group">
                    <label for="apellido">Apellidos</label>
                    <input type="text" class="form-control" id="txtApellido" name="txtApellido" placeholder="apellidos" value="">
                </div>

                 <div class="form-group">
                    <label for="apellido">Celular</label>
                    <input type="text" class="form-control" id="txtCelular" name="txtCelular" placeholder="celular" value="">
                </div>

                <div class="form-group">
                    <label for="txtCorreoElectronico">Correo Electrónico</label>
                    <input type="email" id="txtCorreoElectronico" name="txtCorreoElectronico" autocomplete="off" value="" class="form-control" placeholder="Correo Electrónico">
                    <input type="hidden" name="htdirectorio">
                </div>

                <div class="form-group">
                    <label for="sexo">Sexo</label>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="sexo" id="rbtnSexoMasculino" value="true" checked>
                            Masculino
                        </label>
                    </div>
                      {{csrf_field()}}

                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="sexo" id="rbtnSexoFemenino" value="false">
                            Femenino
                        </label>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="enviarFormulario(event)">Guardar cambios</button>
            </div>
       </div>
    </form>
    </div>
    </div>
</div>

<script>

    $(function () {


        $('#frmRegistar').formValidation(
		{
		    framework: 'bootstrap',
		    excluded: [':disabled', ':hidden', ':not(:visible)', '[class*="notValidate"]'],
		    live: 'enabled',
		    message: '<b style="color: #9d9d9d;">Asegúrese que realmente no necesita este valor.</b>',
		    trigger: null,
		    fields:
			{

			    txtdniRuc:
				{
				    validators:
					{
					    notEmpty:
						{
						    message: '<b style="color: red;">El campo "Nombre" es requerido.</b>'
						}
					}
				},
			    txtNombre:
				{
				    validators:
					{
					    notEmpty:
						{
						    message: '<b style="color: red;">El campo "Nombre" es requerido.</b>'
						}
					}
				},
                 txtApellido:
                {
                    validators:
                    {
                        notEmpty:
                        {
                            message: '<b style="color: red;">El campo "Apellido" es requerido.</b>'
                        }
                    }
                },
                 txtCelular:
                {
                    validators:
                    {
                        notEmpty:
                        {
                            message: '<b style="color: red;">El campo "Apellido" es requerido.</b>'
                        }
                    }
                },

			    txtCorreoElectronico:
				{
				    validators:
					{
					    notEmpty:
						{
						    message: '<b style="color: red;">El campo "Correo electrónico" es requerido.</b>'
						},
					    regexp:
                        {
                            regexp: /^([a-zA-Z0-9\.\-_]+\@[a-zA-Z0-9\-_]+\.[a-zA-Z]+(\.[a-zA-Z]+)?)*$/,
                            message: '<b style="color: red;">El campo "Correo electrónico" no cumple el formato adecuado.</b>'
                        }
					}
				},
			    sexo:
				{
				    validators:
					{
					    notEmpty:
						{
						    message: '<b style="color: red;">El campo "Sexo" es requerido.</b>'
						}
					}
				}
			}
		});
    });

    function enviarFormulario(e) {
        e.preventDefault();

        resetFieldsFrmEditar();

        $('#frmRegistar').data('formValidation').validate();

        if ($('#frmRegistar').data('formValidation').isValid()) {
            swal({
                title: '¿Estas Seguro?',
                text: "Guardar datos del nuevo usuario.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#39843A',
                cancelButtonColor: '#dd4b39',
                confirmButtonText: 'Si, Guardar.'
            }).then(function (confirm) {
                $('#frmRegistar')[0].submit();
            });
        }
    }

    function resetFieldsFrmEditar() {
        $('#frmRegistar').data('formValidation').resetField($('#nombre'));
        $('#frmRegistar').data('formValidation').resetField($('#apellido'));
        $('#frmRegistar').data('formValidation').resetField($('#txtCorreoElectronico'));
        $('#frmRegistar').data('formValidation').resetField($('[name=sexo]'));

    }
</script>
