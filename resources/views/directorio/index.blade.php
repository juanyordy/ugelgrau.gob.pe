@extends('template.templategeneric')
@section('tituloGeneral', 'Directorio')
@section('subTituloGeneral', 'Lista')
@section('cuerpoGeneral')

<div class="col-md-12">
<div class="box box-primary">
  <div class="box-header">
      <div class="row">
          <div class="col-md-9">
              <h3 class="box-title">Lista de directorio</h3>
          </div>
          <div class="col-md-3">
              <div class="form-group">
                  <div class="box-tools">
                      <div class="input-group input-group-sm" style="width: 150px;">
                          <div class="input-group-btn">
                            <input type="button" class="btn btn-success btn-xs" name="RegistrarUsuario" id="RegistrarUsuario" value="Registrar" />
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

    <div class="box-body table-responsive no-padding">
        <table id="example2" class="table table-hover">
            <thead>
                <tr>
                <th>Nombre</th>
                <th>Activo</th>
                <th>Fecha de Registro</th>
                <th>Acciones</th>
                </tr>
            </thead>
             <tbody>

        @foreach ($tdirectorio as $directorio)

            <tr>
            <td>{{ $directorio->nombreapellido }}</td>

            <td>
            	{{  $directorio->fechaRegistro}}</td>
            <td>
            	<a data-parameter="{{ $directorio->codigoarea }}" class="label label-info btn-editar">Editar</a>

          
            </td>
             </tr>
        @endforeach
        </tbody>
        </table>
    </div>
</div>
</div>
<script src="{{ asset('js/AjaxHelper.js') }}"></script>

<script>
    $('.btn-editar').on('click',function(e)
    {
         AjaxHelper.lockScreen();
        $.ajax(
        {
            type:'post',
            dataType:'html',
            data:
                {
                'codigoarea': $(this).data('parameter'),_token : '{{csrf_token()}}'
                },
            url:"{{ url('area/edit') }}",
           success: function (json) {
                AjaxHelper.unLockScreen();

                if (json) {
                    $('#contenedorGeneral').html(json);
                    $('#modal-editar').modal('show');
                }
                else {
                     AjaxHelper.showError();
                }
            },
            error:AjaxHelper.showError

        });
    });
    $('.btn-bloquear').on('click',function()
    {
        AjaxHelper.lockScreen();
        $.ajax(
        {
        type:'post',
            dataType:'html',
            data:
                {
              _token : '{{csrf_token()}}', 'codigoArea': $(this).data('parameter')
                },
             url:"{{ url('area/toblock') }}",
            success: function (json) {
                AjaxHelper.unLockScreen();

                if (json) {
                    $('#contenedorGeneral').html(json);
                    $('#modal-bloquear').modal('show');
                }
                else {
                     AjaxHelper.showError();
                }
            },
            error:AjaxHelper.showError

        });
    });
     $('#RegistrarUsuario').on('click', function () {

        AjaxHelper.lockScreen();

        $.ajax({
            type: 'post',
            dataType: 'html',
            data: {
            _token:'{{csrf_token()}}'
            },
            url: "{{ url('directorio/insert') }}",
            success: function (json) {

                AjaxHelper.unLockScreen();

                if (json) {
                    $('#contenedorGeneral').html(json);
                    $('#modal-insert').modal('show');
                }
                else {
                     AjaxHelper.showError();
                }
            },
            error: AjaxHelper.showError
        });
    });

    $(function () {
      $('#example1').DataTable()
      $('#example2').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      })
    })
</script>
@endsection
