<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Ugel Grau</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="{{asset('plugin/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{asset('plugin/adminlte/bower_components/font-awesome/css/font-awesome.min.css')}}">
	<!-- Ionicons -->
	<link rel="stylesheet" href="{{asset('plugin/adminlte/bower_components/Ionicons/css/ionicons.min.css')}}">
	<!-- Theme style -->
	<link rel="stylesheet" href="{{asset('plugin/adminlte/dist/css/AdminLTE.min.css')}}">
	<link rel="stylesheet" href="{{asset('plugin/sweetalert/sweetalert.css')}}">

	<script src="{{asset('plugin/sweetalert/sweetalert.min.js')}}"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Google Font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
	@if(Session::has('mensajeGlobal'))
		<script>
			swal('{{Session::get('correcto')=='Si' ? 'Correcto' : 'Error en los datos'}}', '{!!Session::get('mensajeGlobal')!!}', '{{Session::get('correcto')=='Si' ? 'success' : 'error'}}');
		</script>
	@endif
	<div class="login-box">
		<div class="login-logo">
			<b>Acceso</b> UGEL-GRAU
		</div>
		<!-- /.login-logo -->
		<div class="login-box-body">
			<p class="login-box-msg">Datos de acceso al sistema</p>
			<form action="{{url('usuario/login')}}" method="post">
				<div class="form-group has-feedback">
					<input type="text" id="txtCorreoElectronico" name="txtCorreoElectronico" class="form-control" placeholder="Correo electrónico" autocomplete="off">
					<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" id="passContrasenia" name="passContrasenia" class="form-control" placeholder="Contraseña">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
					<div class="col-xs-8">
						<a href="{{url('usuario/insertar')}}">Registrarme</a> | 
						<a href="#">Olvidé mi contraseña</a><br>
					</div>
					<!-- /.col -->
					<div class="col-xs-4">
						<button type="submit" class="btn btn-info btn-block btn-flat">Acceder</button>
					</div>
					<!-- /.col -->
				</div>
				{{csrf_field()}}
			</form>
		</div>
		<!-- /.login-box-body -->
	</div>
	<!-- /.login-box -->

	<!-- jQuery 3 -->
	<script src="{{asset('plugin/adminlte/bower_components/jquery/dist/jquery.min.js')}}"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="{{asset('plugin/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
</body>
</html>