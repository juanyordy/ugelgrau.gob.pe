@extends('template.templategeneric')
@section('tituloGeneral', 'anuncios')
@section('subTituloGeneral', 'Anuncios')
@section('cuerpoGeneral')

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">CK Editor<small>Publicar anuncio</small></h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
              <!-- /. tools -->
          </div>
            <!-- /.box-header -->
        <div class="box-body pad">
          <form  id="frmRegistar" action="{{ url('anuncio/insert')  }}" method="post" enctype="multipart/form-data">
            
            <div class="col-md-12">
              <div class="col-md-3"></div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="txtTitulo">Titulo</label>
                  <input type="text" class="form-control" name="txtTitulo">
                </div>
              </div>
            </div>
              <div class="col-md-12">
              <div class="col-md-3"></div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="txtLink">Link</label>
                  <input type="text" class="form-control" name="txtLink">
                </div>
              </div>
            </div>

            
           <div class="col-md-12">
            <div class="col-md-3"></div>
             <div class="col-md-6">
              <div class="form-group">
                <label class="form-label">Descripcion</label>  
                <textarea id="editorDescripcion"  class="form-control" name="txtDescripcion" rows="10" cols="50">
                  
                </textarea>
              </div>
            </div>
              <div class="col-md-3"></div>
           </div>

           <div class="col-md-12">
            <div class="col-md-3"></div>
             <div class="col-md-6">
              <div class="form-group">
                <label class="form-label">Extension</label>  
                <input type="file" name="fileExtencion">
              </div>
            </div>
              <div class="col-md-3"></div>
           </div>
              


            
            <div class="col-md-3"></div> 

            <div class="col-md-10">
              {{ csrf_field() }}
            </div>
              <div class="col-md-2">
                 <div class="form-group">
                 <input type="button" class="btn btn-primary" id="registroUsuario" onclick="enviarFormulario(event)" name="registroUsuario" value="Registrar">
            </div> 
              </div>            
          </form>
            </div>
          </div>
         
        </div>
       
      </div>
     
    </section>


<script>
  $(function () {
   CKEDITOR.replace('editorDescripcion')

    $('.textarea').wysihtml5()


$('#frmRegistar').formValidation(
        {
            framework: 'bootstrap',
            excluded: [':disabled', ':hidden', ':not(:visible)', '[class*="notValidate"]'],
            live: 'enabled',
            message: '<b style="color: #9d9d9d;">Asegúrese que realmente no necesita este valor.</b>',
            trigger: null,
            fields:
            {

                txtTitulo:
                {
                    validators:
                    {
                        notEmpty:
                        {
                            message: '<b style="color: red;">El campo "Titulo" es requerido.</b>'
                        }
                    }
                },
                txtLink:
                
                {
                validators: 
                {
                    notEmpty:
                    {
                         message: '<b style="color: red;">El campo Link es requerido.</b>'
                    },

                }
                },
                  txtDescripcion:
                {
                    validators:
                    {
                        notEmpty:
                        {
                            message: '<b style="color: red;">El campo "Descripcion" es requerido.</b>'
                        }
                    }
                },
                  fileExtencion:
                {
                    validators:
                    {
                        notEmpty:
                        {
                            message: '<b style="color: red;">El campo "Archivo" es requerido.</b>'
                        }
                    }
                },
                txtCuerpo:
                {
                    validators:
                    {
                        notEmpty:
                        {
                            message: '<b style="color: red;">El campo "Cuerpo" es requerido.</b>'
                        }
                    }
                },


            }
        });
    
  })
      function enviarFormulario(e) {
        e.preventDefault();

        resetFieldsFrmInsert();

        $('#frmRegistar').data('formValidation').validate();

        if ($('#frmRegistar').data('formValidation').isValid()) {
            swal({
                title: '¿Estas Seguro?',
                text: "Guardar datos Documento.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#39843A',
                cancelButtonColor: '#dd4b39',
                confirmButtonText: 'Si, Guardar.'
            }).then(function (confirm)
            {
               
                $('#frmRegistar')[0].submit();
            });
        }
    }

    function resetFieldsFrmInsert() {
        $('#frmRegistar').data('formValidation').resetField($('#nombre'));


    }
</script>
@endsection