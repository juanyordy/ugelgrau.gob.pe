@extends('template.templategeneric')
@section('tituloGeneral', 'Oficina')
@section('subTituloGeneral', 'Lista')
@section('cuerpoGeneral')

<div class="col-md-12">
<div class="box box-primary">
  <div class="box-header">
      <div class="row">
          <div class="col-md-9">
              <h3 class="box-title">Anucios</h3>
          </div>
          <div class="col-md-3">
              <div class="form-group">
                  <div class="box-tools">
                      <div class="input-group input-group-sm" style="width: 150px;">
                          <div class="input-group-btn">
                            <a href="{{url('anuncio/insert')}}" class="btn btn-block btn-success btn-flat">Nuevo</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

    <div class="box-body table-responsive no-padding">
        <table id="example2" class="table table-hover">
            <thead>
                <tr>
                <th>Titulo</th>
                <th>Estado</th>
                <th>Importante</th>
                <th>Fecha de Registro</th>
                <th>Acciones</th>
                </tr>
            </thead>
             <tbody>

        @foreach ($tanuncios as $anuncio)

            <tr>
            <td>{{ $anuncio->titulo }}</td>
            <td>
            	<a href="#" class="{{ $anuncio->importante?'label label-success':'label label-danger'}}">{{$anuncio->importante                                                                                          ?'Importante':'Normal' }}</a>
            </td>
              <td>
              <a href="#" class="{{ $anuncio->estado?'label label-success':'label label-warning'}}">{{$anuncio->estado?'Activo':'Inactivo' }}</a>
            </td>
            <td>
            	{{  $anuncio->fecharegistro}}</td>
            <td>
            	<a data-parameter="{{ $anuncio->codigoanuncio }}" class="label label-info btn-editar">Editar</a>

            	<a data-parameter="{{$anuncio->codigoanuncio }}" class="label {{$anuncio->estado?'label-danger':'label-warning'}} btn-bloquear">{{ $anuncio->estado?'Bloquear':'Activar' }}</a>
            </td>
             </tr>
        @endforeach
        </tbody>
        </table>
    </div>
</div>
</div>
<script src="{{ asset('js/AjaxHelper.js') }}"></script>

<script>
    $('.btn-editar').on('click',function(e)
    {
         AjaxHelper.lockScreen();
        $.ajax(
        {
            type:'post',
            dataType:'html',
            data:
                {
                'codigoanuncio': $(this).data('parameter'),_token : '{{csrf_token()}}'
                },
            url:"{{ url('anuncio/edit') }}",
           success: function (json) {
                AjaxHelper.unLockScreen();

                if (json) {
                    $('#contenedorGeneral').html(json);
                    $('#modal-editar').modal('show');
                }
                else {
                     AjaxHelper.showError();
                }
            },
            error:AjaxHelper.showError

        });
    });
    $('.btn-bloquear').on('click',function()
    {
        AjaxHelper.lockScreen();
        $.ajax(
        {
        type:'post',
            dataType:'html',
            data:
                {
              _token : '{{csrf_token()}}', 'codigoanuncio': $(this).data('parameter')
                },
             url:"{{ url('anuncio/bloquear') }}",
            success: function (json) {
                AjaxHelper.unLockScreen();

                if (json) {
                    $('#contenedorGeneral').html(json);
                    $('#modal-bloquear').modal('show');
                }
                else {
                     AjaxHelper.showError();
                }
            },
            error:AjaxHelper.showError

        });
    });
    $(function () {
      $('#example1').DataTable()
      $('#example2').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      })
    })
</script>
@endsection
