@extends('template.templategeneric')
@section('tituloGeneral', 'Carrusel')
@section('subTituloGeneral', 'Slider Carrusel')
@section('cuerpoGeneral')

    <!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">CK Editor<small>Carrusel Slider</small></h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
              <!-- /. tools -->
          </div>
            <!-- /.box-header -->
        <div class="box-body pad">
          <form action="{{ url('slider/insert')}}" method="post" enctype="multipart/form-data">
           
            <div class="col-md-12">
              
              <div class="col-md-6">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="txtTitulo">Titulo</label>
                      <input type="text" class="form-control" name="txtTitulo">
                    </div>
                  </div>
                <div class="col-md-12">                               
                  <div class="form-group">
                    <label class="form-label">Extension</label>  
                    <input type="file" name="fileExtencion">
                  </div>
                </div>


              </div>
              <div class="col-md-6">
                             <div class="col-md-12">
              <div class="form-group">
                <label class="form-label">Descripcion</label>  
                <textarea id="editorDescripcion"  class="form-control" name="txtDescripcion" rows="10" cols="50">
                  
                </textarea>
              </div>
            </div>
              </div>  
            </div>
            


              
            <div class="col-md-3"></div>


            <div class="col-md-3"></div> 

            <div class="col-md-10">
              {{ csrf_field() }}
            </div>
              <div class="col-md-2">
                 <div class="form-group">
                  <button class="btn btn-primary">Guardar</button> 
            </div> 
              </div>            
          </form>
            </div>
          </div>
         
        </div>
       
      </div>
     
    </section>


<script>
  $(function () {
   CKEDITOR.replace('editorDescripcion')
    CKEDITOR.replace('editorContenido')

    $('.textarea').wysihtml5()

    
  })
</script>
@endsection

