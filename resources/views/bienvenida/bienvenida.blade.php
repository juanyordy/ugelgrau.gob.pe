@extends('template.templateprincipal')
@section('cuerpoGeneral')

<section class="pages pages-ugel">
	<article class="container main-content">
	
	<div class="header_top">
	
	<div class="container">
		<header>
			<h1 class="title">UgeL Grau</h1>
			
		</header>
		
		<div class="breadcrumb">
			<a href="#">Inicio</a> &gt;
				<a href="#">UGEL GRAU</a> &gt;
			
			
				<span class="sombra_bread">Bienvenida</span>
		</div>

	</div>

	</div>

	
	<div class="row">
		
		<div class="col-md-3">
		<ul class="menu-lateral">
			<li class="item current"><a href="#" title="Bienvenida">Bienvenida</a></li>
				<li class="item "><a href="#" title="Organigrama">Organigrama</a></li>
				<li class="item "><a href="#" title="Jurisdicción">Jurisdicción</a></li>
				<li class="item "><a href="#" title="Presentación">Presentación</a></li>
		</ul>
		</div>

		
		<div class="col-md-9">
		<h2 class="main_title">Bienvenida</h2>
		
		<div class="post-content">
			
	
	<div style="width: 640px; " class="wp-video"><!--[if lt IE 9]><script>document.createElement('video');</script><![endif]-->
<span class="mejs-offscreen">Bienvenida</span>
<div id="mep_0" class="mejs-container svg wp-video-shortcode mejs-video" tabindex="0" role="application" aria-label="Video Player" style="width: 640px; height: 360px;">
	<div class="mejs-inner">
		<div class="mejs-mediaelement">
			<img class="wp-video-shortcode" id="video-4494-1" width="640" height="360" autoplay="1" preload="metadata" src="{{asset('imagenes/bienvenida/5aff4eb9edb54.jpg')}}" style="width: 100%; height: 100%;"><source type="video/mp4" src="{{asset('imagenes/bienvenida/5aff4eb9edb54.jpg')}}"><a href=""></a>
			</div>

	


</div>
</div>
</div>

{{$tbienvenida->descripcion}}
				    </div>

			    </div>

		    </div>

		</article>
    </section>
    @endsection