@extends('template.templateprincipal')
@section('cuerpoGeneral')

<section class="pages">
		<article class="container main-content">
			<div class="header_top">
	<div class="container">
		<header>
							<h1 class="title">{{$publicacion->titulo}}</h1>
			
		</header>
		<div class="breadcrumb">
			<a href="http://www.ugel01.gob.pe">Inicio</a> &gt; <a href="http://www.ugel01.gob.pe/noticias/">Publicaciones</a>		</div>
	</div>
</div>
	<div class="row">
		<div class="col-md-12">
			<div class="post-content single-content">
						<img src="{{asset('imagenes/publicaciones').'/'.$publicacion->codigopublicacion.'.'.$publicacion->extensionportada}}" alt="{{$publicacion->titulo}}" class="main-imagen">
						{!!$publicacion->cuerpo!!}


						<a href="{{$publicacion->link}}" class="more-link"><span>Ver link</span> <i class="mas"> </i></a>

			</div>
			<div class="controles">
			 <a href="#" rel="next">&lt;&lt;
			 		Anterior</a>
			 		 <a href="#" rel="prev">Siguiente &gt;&gt;</a>
			</div>
		</div>
	</div>
		</article>
	</section>
@endsection