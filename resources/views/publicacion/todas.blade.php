@extends('template.templateprincipal')
@section('cuerpoGeneral');
<style>
.img-rounded {
  border-radius: 6px;
}
.img-thumbnail {
  display: inline-block;
  max-width: 100%;
  height: auto;
  padding: 4px;
  line-height: 1.42857143;
  background-color: #fff;
  border: 1px solid #ddd;
  border-radius: 4px;
  -webkit-transition: all .2s ease-in-out;
       -o-transition: all .2s ease-in-out;
          transition: all .2s ease-in-out;
}
.img-circle {
  border-radius: 50%;
}
figure {
  margin: 1em 40px;
}
figure {
  margin: 0;
}
figure{
  display: block;
}
.pagination {
  display: inline-block;
  padding-left: 0;
  margin: 20px 0;
  border-radius: 4px;
}
.pagination > li {
  display: inline;
}
.pagination > li > a,
.pagination > li > span {
  position: relative;
  float: left;
  padding: 6px 12px;
  margin-left: -1px;
  line-height: 1.42857143;
  color: #337ab7;
  text-decoration: none;
  background-color: #fff;
  border: 1px solid #ddd;
}
.pagination > li:first-child > a,
.pagination > li:first-child > span {
  margin-left: 0;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}
.pagination > li:last-child > a,
.pagination > li:last-child > span {
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
}
.pagination > li > a:hover,
.pagination > li > span:hover,
.pagination > li > a:focus,
.pagination > li > span:focus {
  z-index: 2;
  color: #23527c;
  background-color: #eee;
  border-color: #ddd;
}
.pagination > .active > a,
.pagination > .active > span,
.pagination > .active > a:hover,
.pagination > .active > span:hover,
.pagination > .active > a:focus,
.pagination > .active > span:focus {
  z-index: 3;
  color: #fff;
  cursor: default;
  background-color: #337ab7;
  border-color: #337ab7;
}
.pagination > .disabled > span,
.pagination > .disabled > span:hover,
.pagination > .disabled > span:focus,
.pagination > .disabled > a,
.pagination > .disabled > a:hover,
.pagination > .disabled > a:focus {
  color: #777;
  cursor: not-allowed;
  background-color: #fff;
  border-color: #ddd;
}
.pagination-lg > li > a,
.pagination-lg > li > span {
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.3333333;
}
.pagination-lg > li:first-child > a,
.pagination-lg > li:first-child > span {
  border-top-left-radius: 6px;
  border-bottom-left-radius: 6px;
}
.pagination-lg > li:last-child > a,
.pagination-lg > li:last-child > span {
  border-top-right-radius: 6px;
  border-bottom-right-radius: 6px;
}
.pagination-sm > li > a,
.pagination-sm > li > span {
  padding: 5px 10px;
  font-size: 12px;
  line-height: 1.5;
}
.pagination-sm > li:first-child > a,
.pagination-sm > li:first-child > span {
  border-top-left-radius: 3px;
  border-bottom-left-radius: 3px;
}
.pagination-sm > li:last-child > a,
.pagination-sm > li:last-child > span {
  border-top-right-radius: 3px;
  border-bottom-right-radius: 3px;
}
</style>
<div class="to_menu">&nbsp;</div>

<section class="pages pages-noticias">
	<article class="container main-content">
		<div class="header_top">
			<div class="container">
				<header>
				<h1 class="title">Publicaciones</h1>
				</header>
		<div class="breadcrumb">

			<a href="https://www.ugelgrau.gob.pe/">Inicio</a> &gt;
			<span class="sombra_bread">Publicaciones</span>
		</div>
	</div>
</div>	
<div class="row">


		<div class="col-md-9">
			<div class="post-content">
				<div class="contenido">

				<ul class="lista_noticias">
					@foreach($tPublicacion as $publicacion)
					<li class="noticia-home">
						<div class="col-md-6">
							
							<figure class="figure"><div class="imagen">

								<a href="#" title="#"">
									
									<img class="img-thumbnail" src="{{asset('imagenes/publicaciones').'/'.$publicacion->codigopublicacion.'.'.$publicacion->extensionportada}}" alt="#"></a>
									
							</div>
							</figure>
						</div>
						<div class="col-md-6">
							<div class="nTitulo"><a href="#" title="">{{$publicacion->titulo}}</a></div>
							<div class="fecha">{{$publicacion->fecharegistro}}</div>
							<div class="dContent">

								 {!!$publicacion->descripcion!!}

											
								<a href="{{url('publicacion/vermas').'/'.$publicacion->codigopublicacion}}"" class="more-link"><span>Continuar Leyendo</span> <i class="mas"> </i></a>
							</div>
						</div>
						</li>
						@endforeach			
							<ul class="pagination">
    						{{$tPublicacion->links()}}
							</ul>
													
					</ul>
						</div>
					</div>
				</div>

		<div class="col-md-3">
			<ul class="menu-lateral">

				<li class="item current"><a href="#" title="Publicaciones">Publicaciones</a></li>
			</ul>
		</div>
			</div>
		</article>
	</section>

@endsection