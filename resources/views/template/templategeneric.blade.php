<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <title>UGEL-GRAU</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="{{asset('plugin/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{asset('plugin/adminlte/bower_components/font-awesome/css/font-awesome.min.css')}}">
	<!-- Ionicons -->
	<link rel="stylesheet" href="{{asset('plugin/adminlte/bower_components/Ionicons/css/ionicons.min.css')}}">

	<!-- Theme style -->
	<link rel="stylesheet" href="{{asset('plugin/adminlte/dist/css/AdminLTE.min.css')}}">
	<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="{{asset('plugin/adminlte/dist/css/skins/_all-skins.min.css')}}">
	 <link rel="stylesheet" href="{{asset('plugin/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
	<!-- Morris chart -->
	<link rel="stylesheet" href="{{asset('plugin/adminlte/bower_components/morris.js/morris.css')}}">
	<!-- jvectormap -->
	<link rel="stylesheet" href="{{asset('plugin/adminlte/bower_components/jvectormap/jquery-jvectormap.css')}}">
	<!-- Date Picker -->
	<link rel="stylesheet" href="{{asset('plugin/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="{{asset('plugin/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Google Font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

	<link rel="stylesheet" href="{{asset('/plugin/sweetalert/sweetalert.css')}}">


	<script src="{{asset('plugin/sweetalert/sweetalert.min.js')}}"></script>

	<!-- jQuery 3 -->
	<script src="{{asset('plugin/adminlte/bower_components/jquery/dist/jquery.min.js')}}"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="{{asset('plugin/adminlte/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
	<script src="{{asset('js//jquery.bootstrap-growl.min.js')}}"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

	<header class="main-header">
	<!-- Logo -->
	<a href="#" class="logo">
	<!-- mini logo for sidebar mini 50x50 pixels -->
	<span class="logo-mini">Pagina Ugel Grau</span>
	<!-- logo for regular state and mobile devices -->
	<span class="logo-lg"><b>UGEL</b>GRAU</span>
	
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top">
	<!-- Sidebar toggle button-->
	<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
	<span class="sr-only">Navegacion</span>
	</a>

	<div class="navbar-custom-menu">
	<ul class="nav navbar-nav">
	<!-- Notifications: style can be found in dropdown.less -->
	<li class="dropdown notifications-menu" style="border-left: 1px solid #4094c4;">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
		<i class="fa fa-bell-o"></i>
		<span class="label label-warning">10</span>
		</a>
		<ul class="dropdown-menu">
			<li class="header">Tienes 10 notificaciones</li>
			<li>
				<!-- inner menu: contains the actual data -->
				<ul class="menu">
					<li>
					<a href="#">
					<i class="fa fa-users text-aqua"></i> 5 new members joined today
					</a>
					</li>
				</ul>
			</li>
			<li class="footer"><a href="#">View all</a></li>
		</ul>
	</li>
	<!-- User Account: style can be found in dropdown.less -->
	<li class="dropdown user user-menu" style="border-left: 1px solid #4094c4;">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown">
	<img src="{{asset('usuario/avatar/'.Session::get('codigousuario').'.'.Session::get('extensionAvatar'))}}" class="user-image" alt="User Image">
	<span class="hidden-xs">{{Session::get('correoElectronico')}}</span>
	</a>
	<ul class="dropdown-menu">
	<!-- User image -->
	<li class="user-header">
	<img src="{{asset('usuario/avatar/'.Session::get('codigousuario').'.'.Session::get('extensionAvatar'))}}" class="img-circle" alt="User Image">

	<p>
	{{Session::get('nombre')}}
	<small>{{Session::get('rol')}}</small>
	

	</p>
	</li>
	<!-- Menu Footer-->
	<li class="user-footer">
	<div class="pull-left">
	<a href="{{url('usuario/editar')}}" class="btn btn-default btn-flat">Perfil</a>
	</div>
	<div class="pull-right">
	<a href="{{url('usuario/logout')}}" class="btn btn-default btn-flat">Salir</a>
	</div>
	</li>
	</ul>
	</li>
	<!-- Control Sidebar Toggle Button -->
	<li style="border-left: 1px solid #cccccc;">
		<a href="{{url('usuario/logout')}}"><i class="fa fa-sign-out"></i></a>
	</li>
	</ul>
	</div>
	</nav>
	</header>
	<!-- Left side column. contains the logo and sidebar -->
	<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
	<!-- Sidebar user panel -->
	<div class="user-panel">
	<div class="pull-left image">
	<img src="{{asset('usuario/avatar/'.Session::get('codigousuario').'.'.Session::get('extensionAvatar'))}}" class="img-circle" alt="User Image">
	</div>
	<div class="pull-left info">
	<p>{{Session::get('nombre')}}</p>
	<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
	</div>
	</div>
	<!-- search form -->
	<form action="#" method="get" class="sidebar-form">
	<div class="input-group">
	<input type="text" name="q" class="form-control" placeholder="Buscar...">
	<span class="input-group-btn">
	<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
	</button>
	</span>
	</div>
	</form>
	<!-- /.search form -->
	<!-- sidebar menu: : style can be found in sidebar.less -->
	<ul class="sidebar-menu" data-widget="tree">
     <li class="header">MAIN NAVIGATION</li>
     <li><a href="{{ url('/')}}"><i class="fa fa-home"></i> <span>Inicio</span></a></li>
 	@if(strpos(Session::get('rol'),"superusuario")!==false)
 	<li class="treeview">
       <a href="#">
           <i class="fa fa-gears"></i> <span>Configuracion</span>
           <span class="pull-right-container">
               <i class="fa fa-angle-left pull-right"></i>
           </span>
       </a>
     	<ul class="treeview-menu">

        <li class="active"> <a href="{{ url('usuario/listar') }}">  <i class="fa fa-circle-o"></i>Lista usuarios</a> </li>
        <li class="active"> <a href="{{ url('asignacionarea/index') }}">  <i class="fa fa-circle-o"></i>Usuarios asignados</a> </li>

        <li class="active"> <a href="{{ url('periodo/insert') }}">  <i class="fa fa-circle-o"></i>Registar periodo</a> </li>
        <li class="active"> <a href="{{ url('periodo/index') }}">  <i class="fa fa-circle-o"></i>Periodos registrados</a> </li>
        </ul>
    </li>
    @endif

    <li class="treeview">
        <a href="#">
            <i class="fa fa-newspaper-o"></i> <span>Mis Datos</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="active"><a href="{{ url('usuario/editar') }}"><i class="fa fa-circle-o"></i>Editar mis datos</a></li>
        </ul>
    </li>
  @if(strpos(Session::get('rol'),'superusuario')!==false)
     <li class="treeview">
         <a href="#">
             <i class="fa fa-university"></i> <span>Slider</span>
             <span class="pull-right-container">
                 <i class="fa fa-angle-left pull-right"></i>
             </span>
         </a>

        <ul class="treeview-menu">
            <li class="active"> <a href="{{ url('slider/insert') }}">  <i class="fa fa-plus-square"></i>Registrar Slider</a> </li>
            <li class="active"> <a href="{{ url('slider/index') }}">  <i class="fa  fa-list-alt"></i>Slider</a> </li>
        </ul>
    </li>
   @endif
    <li class="treeview">
        <a href="#">
            <i class="fa  fa-file-text"></i> <span>Publicaciones</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
		@if(strpos(Session::get('rol'),'superusuario')!==false)
        	<li class="active">
        	 <a href="{{ url('publicacion/insert') }}">  <i class="fa fa-plus-square"></i>Registrar Publicacion</a> </li>
        @endif
        	<li class="active">
        		<a href="{{url('publicacion/index')}}"><i class="fa fa-plus-square"></i>Publicaciones</a>
        	</li>

        </ul>
    </li>
    @if(strpos(Session::get('rol'),'superusuario')!==false)
    <li class="treeview">
       <a href="#">
           <i class="fa fa-file-text-o"></i> <span>Directorio</span>
           <span class="pull-right-container">
               <i class="fa fa-angle-left pull-right"></i>
           </span>
       </a>

     	<ul class="treeview-menu">

        <li class="active"> <a href="{{ url('tipotramite/insert') }}">  <i class="fa fa-plus-square"></i>Registrar Directorio</a> </li>
        
        <li class="active"> <a href="{{ url('tipotramite/index') }}">  <i class="fa  fa-list-alt"></i>Directorio</a> </li>

        </ul>

    </li>
  @endif

	@if(strpos(Session::get('rol'),'superusuario')!==false)
	<li class="treeview">
		 <a href="#">
				 <i class="fa fa-file-text-o"></i> <span>Anuncio</span>
				 <span class="pull-right-container">
						 <i class="fa fa-angle-left pull-right"></i>
				 </span>
		 </a>
		<ul class="treeview-menu">
			<li class="active"> <a href="{{ url('anuncio/insert') }}">  <i class="fa fa-plus-square"></i>Registrar Anuncio</a> </li>
			<li class="active"> <a href="{{ url('anuncio/index') }}">  <i class="fa fa-plus-square"></i>Anuncios</a> </li>


			</ul>
	</li>
@endif
     <li><a><i></i> <span></span></a></li>
</ul>
	</section>
	<!-- /.sidebar -->
	</aside>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">

	@yield('tituloGeneral')
	<small>@yield('subTituloGeneral')</small>
	
	</section>


	<!-- Main content -->
	<section class="content">
	@if(Session::has('mensajeGlobal'))
	<script>
	$(function ()
	{
        var mensaje ='{!!Session::get('mensajeGlobal')!!}';


        	if ('{{Session::get('correcto')}}')
        	{
        		$.bootstrapGrowl( mensaje,
           		{
                	type:'info',
                	delay: 3000
            	});
        	}
        	else
        	{
        		$.bootstrapGrowl(mensaje,
           		{
                	type:'danger',
                	delay: 3000
            	});
        	}
    });
	</script>
@endif
		<!-- Main row -->
		<div class="row">
			<div id="contenedorGeneral"></div>
			@yield('cuerpoGeneral')
		</div>
		<!-- /.row (main row) -->
	</section>
	<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
	<footer class="main-footer">
	<div class="pull-right hidden-xs">
	<b>Version</b> 1.0.0
	</div>
	<strong>Copyright &copy; 2017-{{date('Y')}} <a href="http://codideep.com">yordy</a>.</strong> Todos los derechos reservados.
	</footer>
	<!-- /.control-sidebar -->
	<!-- Add the sidebar's background. This div must be placed
	immediately after the control sidebar -->
	<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
		$.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.7 -->
	<script src="{{asset('plugin/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('plugin/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('plugin/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
	<script src="{{asset('js//jquery.bootstrap-growl.min.js')}}"></script>
	<!-- Morris.js charts -->
	<script src="{{asset('plugin/adminlte/bower_components/Chart.js/Chart.js')}}"></script>
	<script src="{{asset('plugin/adminlte/bower_components/raphael/raphael.min.js')}}"></script>
	<script src="{{asset('plugin/adminlte/bower_components/morris.js/morris.min.js')}}"></script>
	<!-- Sparkline -->
	<script src="{{asset('plugin/adminlte/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
	<!-- jvectormap -->
	<script src="{{asset('plugin/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
	<script src="{{asset('plugin/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
	<!-- jQuery Knob Chart -->
	<script src="{{asset('plugin/adminlte/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
	
	<!-- daterangepicker -->
	<script src="{{asset('plugin/adminlte/bower_components/moment/min/moment.min.js')}}"></script>
	<script src="{{asset('plugin/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
	<!-- datepicker -->
	<script src="{{asset('plugin/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script src="{{asset('plugin/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
	<!-- Slimscroll -->
	<script src="{{asset('plugin/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
	<!-- FastClick -->
	<script src="{{asset('plugin/adminlte/bower_components/fastclick/lib/fastclick.js')}}"></script>
	<script src="{{asset('plugin/adminlte/bower_components/ckeditor/ckeditor.js')}}"></script>
	<!-- AdminLTE App -->
	<script src="{{asset('plugin/adminlte/dist/js/adminlte.min.js')}}"></script>

	<script src="{{asset('plugin/formvalidation/formValidation.min.js')}}"></script>
    <script src="{{asset('plugin/formvalidation/bootstrap.validation.min.js')}}"></script>
 
	<script>
		$(function()
		{
			$('input').attr('autocomplete', 'off');
		});
	</script>
</body>
</html>
