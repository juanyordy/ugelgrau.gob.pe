<!DOCTYPE html>
<!-- saved from url=(0026)https://www.ugelgrau.gob.pe/ -->
<html lang="es" class="">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <!--<meta content='width=device-width, target-densitydpi=160, initial-scale=1' name='viewport'>-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<link rel="shortcut icon" type="image/png" href="{{asset('imagenes/logo/logo.png')}}">
	<title>Ugel Grau | Unidad de gestion Educativa Local 304</title>
    


      <script async="" src="https://www.google-analytics.com/analytics.js"></script>
      
      <script type="application/javascript">

    </script>

  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:700" rel="stylesheet" type="text/css">

  <link href="{{asset('plugins/wp-content/themes/style.css')}}" rel="stylesheet" type="text/css" media="screen">
  
    
 

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="{{asset('plugins/wp-includes/wp-emoji-release.min.js')}}" type="text/javascript">
      
    </script>

<style type="text/css">

  img.wp-smiley,
  img.emoji {
  	display: inline !important;
  	border: none !important;
  	box-shadow: none !important;
  	height: 1em !important;
  	width: 1em !important;
  	margin: 0 .07em !important;
  	vertical-align: -0.1em !important;
  	background: none !important;
  	padding: 0 !important;
  }
</style>

<style>
.img-rounded {
  border-radius: 6px;
}
.img-thumbnail {
  display: inline-block;
  max-width: 100%;
  height: auto;
  padding: 4px;
  line-height: 1.42857143;
  background-color: #fff;
  border: 1px solid #ddd;
  border-radius: 4px;
  -webkit-transition: all .2s ease-in-out;
       -o-transition: all .2s ease-in-out;
          transition: all .2s ease-in-out;
}
.img-circle {
  border-radius: 50%;
}
figure {
  margin: 1em 40px;
}
figure {
  margin: 0;
}
figure{
  display: block;
}
.pagination {
  display: inline-block;
  padding-left: 0;
  margin: 20px 0;
  border-radius: 4px;
}
.pagination > li {
  display: inline;
}
.pagination > li > a,
.pagination > li > span {
  position: relative;
  float: left;
  padding: 6px 12px;
  margin-left: -1px;
  line-height: 1.42857143;
  color: #337ab7;
  text-decoration: none;
  background-color: #fff;
  border: 1px solid #ddd;
}
.pagination > li:first-child > a,
.pagination > li:first-child > span {
  margin-left: 0;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}
.pagination > li:last-child > a,
.pagination > li:last-child > span {
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
}
.pagination > li > a:hover,
.pagination > li > span:hover,
.pagination > li > a:focus,
.pagination > li > span:focus {
  z-index: 2;
  color: #23527c;
  background-color: #eee;
  border-color: #ddd;
}
.pagination > .active > a,
.pagination > .active > span,
.pagination > .active > a:hover,
.pagination > .active > span:hover,
.pagination > .active > a:focus,
.pagination > .active > span:focus {
  z-index: 3;
  color: #fff;
  cursor: default;
  background-color: #337ab7;
  border-color: #337ab7;
}
.pagination > .disabled > span,
.pagination > .disabled > span:hover,
.pagination > .disabled > span:focus,
.pagination > .disabled > a,
.pagination > .disabled > a:hover,
.pagination > .disabled > a:focus {
  color: #777;
  cursor: not-allowed;
  background-color: #fff;
  border-color: #ddd;
}
.pagination-lg > li > a,
.pagination-lg > li > span {
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.3333333;
}
.pagination-lg > li:first-child > a,
.pagination-lg > li:first-child > span {
  border-top-left-radius: 6px;
  border-bottom-left-radius: 6px;
}
.pagination-lg > li:last-child > a,
.pagination-lg > li:last-child > span {
  border-top-right-radius: 6px;
  border-bottom-right-radius: 6px;
}
.pagination-sm > li > a,
.pagination-sm > li > span {
  padding: 5px 10px;
  font-size: 12px;
  line-height: 1.5;
}
.pagination-sm > li:first-child > a,
.pagination-sm > li:first-child > span {
  border-top-left-radius: 3px;
  border-bottom-left-radius: 3px;
}
.pagination-sm > li:last-child > a,
.pagination-sm > li:last-child > span {
  border-top-right-radius: 3px;
  border-bottom-right-radius: 3px;
}
</style>

<link rel="stylesheet" id="ai1ec_style-css" href="{{asset('ugel/1a9d0098_ai1ec_parsed_css.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="contact-form-7-css" href="{{asset('plugins/wp-content/themes/style.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="rs-plugin-settings-css" href="{{asset('plugins/wp-content/plugins/revslider/css/settings.css')}}" type="text/css" media="all">

<style id="rs-plugin-settings-inline-css" type="text/css">
.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
</style>

<link rel="stylesheet" id="owl-carousel-css" href="{{asset('OwlCarousel/dist/assets/owl.carousel.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="owl.theme.css-css" href="{{asset('OwlCarousel/dist/assets/owl.theme.green.min.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="wpmu-wpmu-ui-3-min-css-css" href="{{asset('plugins/wp-content/plugins/wordpress/in/css/wpmu-ui.3.min.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="wpmu-animate-3-min-css-css" href="{{asset('plugins/wp-content/plugins/wordpress/in/css/animate.3.min.css')}}" type="text/css" media="all">
<script type="text/javascript" src="{{asset('OwlCarousel/docs/assets/vendors/jquery.min.js')}}"></script>


<script type="text/javascript" src="{{asset('OwlCarousel/dist/owl.carousel.min.js')}}"></script>
<link rel="https://api.w.org/" href="#">
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="#">
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="#"> 


</head>
<body>
<header class="main-header vFixed" id="block_top_menu">
	<div class="franja_top">
		<div class="container">
     
			<div class="row">
				<div class="col-md-9 franja_top_contenedor">

					<span class="central">Central Telefónica: <a href="tel:017191890" title="Central Telefónica" class="es_link">RPC.973818169</a></span>

					<span class="correo">Correo: <a href="mailto:ugelgrau304@gmail.com" title="Correo Principal">ugelgrau304@gmail.com</a></span>
					<span class="horario">Atención al ciudadano: Lun - Vi 8:30 am a 5:30 pm</span>

				</div>
				<div class="col-md-3 links_top">


					<a href="{{url('/')}}" title="Inicio" class="inicio">INICIO</a>
					<a href="#" title="Contacto" class="contacto">CONTACTO</a>
					<a href="#" title="Webmail" rel="nofollow" target="_blank" class="webmail">WEBMAIL</a>
				</div>
			</div>
		</div>
	</div>

	<div class="container header_principal">
		<div class="row">
			<div class="col-md-6 col-xs-3">
				<div class="dLogo">
					<a href="#" title="Ugel Grau">
            <img class="img-thumbnail" src="{{asset('imagenes/logo/ugelgrau.png')}}" alt="Logo Ugel Grau"></a>
				</div>

        </div>
			<div class="col-md-6 col-xs-6">
				<div class="row">
					<div class="col-md-12  links_menu">
            
            <div class="col-sm-2"> 
                <a href="http://ugelgrau.sapp-peru.com/" title="Sapp">  
                <img  class="img-thumbnail" src="{{asset('imagenes/tramitedoc/sapp.png')}}" alt="Sapp" width="50" height="150">
                </a>

              <a href="https://tramitedocumentario.ugelgrau.gob.pe" target="_blank" title="Tramite Documentario ">
                <img  class="img-thumbnail" src="{{asset('imagenes/tramitedoc/tramitedocumetario.png')}}" alt="Tramitedocumetario" width="120" height="40">
              </a>

            </div>
      
        

					</div>

					<div class="col-md-12">
						<div id="triger_menu" class="triger_menu" style="display: none;">
							<span></span>
							<span></span>
							<span></span>
						</div>
						<nav class="dwMenu" id="dwMenu">
							<div class="menu-menu-principal-container">
                <ul id="menu-principal" class="nav navbar-nav">
    <li id="menu-item-22" class="fondo_verde menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-22"><a href="https://www.ugelgrau.gob.pe/">Ugel Grau</a>

  <ul class="sub-menu">
	 <li id="menu-item-4495" class="menu-item menu-item-type-post_type menu-item-object-ugel menu-item-4495">
        <a href="{{url('bienvenida')}}">Bienvenida</a>
  </li>
	<li id="menu-item-4496" class="menu-item menu-item-type-post_type menu-item-object-ugel menu-item-4496">
    <a href="#">Organigrama</a>
  </li>
	<li id="menu-item-5025" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5025">
    <a href="{{url('directorio')}}">Directorio</a>
  </li>
	<li id="menu-item-5592" class="menu-item menu-item-type-post_type menu-item-object-ugel menu-item-5592">
    <a href="#">Directorio Telefónico</a>
  </li>
	<li id="menu-item-4497" class="menu-item menu-item-type-post_type menu-item-object-ugel menu-item-4497">
    <a href="#">Jurisdicción</a>
  </li>
</ul>

</li>

<li id="menu-item-4510" class="fondo_rojo menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4510">
  <a href="#">Estructura Orgánica</a>
<ul class="sub-menu">
	<li id="menu-item-4498" class="fondo_rojo menu-item menu-item-type-post_type menu-item-object-estructura-organica menu-item-has-children menu-item-4498">
    <a href="#">Dirección</a>
	<ul class="sub-menu">
		<li id="menu-item-4499" class="menu-item menu-item-type-post_type menu-item-object-estructura-organica menu-item-4499">
      <a href="#">Control</a>
    </li>
	</ul>

</li>
	<li id="menu-item-4500" class="menu-item menu-item-type-post_type menu-item-object-estructura-organica menu-item-has-children menu-item-4500">
    <a href="#">Asesoramiento</a>
	<ul class="sub-menu">
		<li id="menu-item-4501" class="menu-item menu-item-type-post_type menu-item-object-estructura-organica menu-item-4501">
      <a href="#">Área de Asesoría Jurídica</a>
    </li>
		<li id="menu-item-4502" class="menu-item menu-item-type-post_type menu-item-object-estructura-organica menu-item-4502">
      <a href="#">Área de Planificación y Presupuesto</a>
    </li>
	</ul>

</li>
	<li id="menu-item-4503" class="menu-item menu-item-type-post_type menu-item-object-estructura-organica menu-item-has-children menu-item-4503">
    <a href="#">Apoyo</a>
	<ul class="sub-menu">
		<li id="menu-item-4504" class="menu-item menu-item-type-post_type menu-item-object-estructura-organica menu-item-4504">
      <a href="#">Área de Administración</a>
    </li>
		<li id="menu-item-4505" class="menu-item menu-item-type-post_type menu-item-object-estructura-organica menu-item-4505">
      <a href="#">Área de Recursos Humanos</a>
    </li>
	</ul>

</li>
	<li id="menu-item-4506" class="menu-item menu-item-type-post_type menu-item-object-estructura-organica menu-item-has-children menu-item-4506">
    <a href="#">Línea</a>
	<ul class="sub-menu">
		<li id="menu-item-4507" class="menu-item menu-item-type-post_type menu-item-object-estructura-organica menu-item-4507">
      <a href="#/">Área de Gestión de la Educación Básica Regular y Especial</a>
    </li>
		<li id="menu-item-4508" class="menu-item menu-item-type-post_type menu-item-object-estructura-organica menu-item-4508">
      <a href="#">Área de Gestión de la Educación Básica Alternativa y Técnico Productiva</a>
    </li>
		<li id="menu-item-4509" class="menu-item menu-item-type-post_type menu-item-object-estructura-organica menu-item-4509">
      <a href="#">Área de Supervisión y Gestión del Servicio Educativo</a>
    </li>
	</ul>

</li>
</ul>

</li>
<li id="menu-item-20" class="fondo_morado menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-20">
  <a href="#">Servicios</a>
<ul class="sub-menu">
	<li id="menu-item-5012" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5012">
    <a href="#">Noticias</a>
  </li>
	<li id="menu-item-5014" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5014">
    <a href="#">Normas, Directivas y Resoluciones</a>
  </li>
	<li id="menu-item-5015" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5015">
    <a href="#">Documentos Generales</a>
  </li>
	<li id="menu-item-5744" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5744">
    <a target="_blank" href="#">Copia de Resoluciones Directorales</a>
  </li>
	<li id="menu-item-5019" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5019">
    <a href="#">Avisos de Tesoreria</a>
  </li>
	<li id="menu-item-5018" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5018">
    <a href="#">Cronograma de Cheques</a>
  </li>
	<li id="menu-item-5011" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5011">
    <a href="#">FUT</a>
  </li>
	<li id="menu-item-5013" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5013">
    <a href="#">Revista Digital</a>
  </li>
	<li id="menu-item-5114" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5114">
    <a target="_blank" href="#">Plataforma Virtual</a>
  </li>
	<li id="menu-item-5062" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5062">
    <a href="#">AIP Actualización de Información 2016</a>
  </li>
	<li id="menu-item-5065" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5065">
    <a href="#">Evaluación Docente</a>
  </li>
</ul>

</li>
<li id="menu-item-19" class="fondo_azul menu-item menu-item-type-post_type menu-item-object-page menu-item-19">
  <a href="#">Ciudadano</a>
</li>
</ul></div>						</nav>
					</div>
				</div>

			</div>
		</div>
	</div>

	<script type="text/javascript">

      $(function()
      {
          $("#triger_menu").click(function(){
        $("#dwMenu").slideToggle();
      });

      $("#menu_footer").click(function(){
        var menufooter=$(".menu-menu-footer-container");
        menufooter.slideToggle(function(){
          if(menufooter.is(':visible'))
          {
            $("#menu_footer").addClass('showmenos');
          }
          else
          {
            $("#menu_footer").removeClass('showmenos');
          }
        });


      });

      var pWidth = Math.max( document.documentElement.clientWidth , window.innerWidth );

      $(window).bind( 'resize' , function(event)
      {
        if (pWidth != Math.max( document.documentElement.clientWidth , window.innerWidth ))
        {
          do_responsive();
        }
        pWidth = Math.max( document.documentElement.clientWidth , window.innerWidth );

      });

      window.onorientationchange = function() {
        do_responsive();
      }

      do_responsive();

      function do_responsive()
      {
        var windowWidth = Math.max( document.documentElement.clientWidth , window.innerWidth );
        if( windowWidth < 768) {
          $("#dwMenu").hide();
          $("#triger_menu").show();
          $(".menu-menu-footer-container").hide();
        }

        else
        {
          $("#dwMenu").show();
          $("#triger_menu").hide();
          $(".menu-menu-footer-container").show();
        }
      }

        /*$(window ).bind( 'scroll', function () {
        var navHeight = 140;
        if ($(window). scrollTop() > navHeight) {
          $('#block_top_menu').addClass('vFixed' );
        }
        else {
          $('#block_top_menu').removeClass('vFixed' );
        }
      });*/

    });


		


		

	</script>
</header>
<div class="to_menu">&nbsp;</div>
<div id="cuerpo">
  @yield('cuerpoGeneral')
</div>
  
	<script>

      $("#dCarruselGrupo1").owlCarousel ({
        items :6,
        dots:false,
        nav : true,
        navText:["",""],
        loop :true,
        autoplay :true,
        autoplayTimeout :1000,
        autoplayHoverPause :true,
        smartSpeed :450
      });

			$("#dCarruselGrupo2").owlCarousel ({
				items :3,
				dots:false,
				nav : true,
				navText:["",""],
				loop :true,
				autoplay :true,
				autoplayTimeout :1000,
				autoplayHoverPause :true,
				smartSpeed :450
			});


			$("#dCarruselAvisos").owlCarousel ({
				items :1,
				dots:false,
				navText:["",""],
				nav : false,
				loop :true,
				autoplay :true,
				autoplayTimeout :1000,
				autoplayHoverPause :true,
				smartSpeed :450
			})


	</script>

<footer>
	<div class="footer1">
		<div class="">
			<div class="row">
				<div class="col-md-6 col-xs-12" >
					<div class="FooterA">
						<a href="#" title="Inicio"><img src="{{asset('imagenes/logo/logo.png')}}" class="logo_footer" alt="Logo Footer Ugel01"></a>
						<div class="info_footer">
							<h3>UNIDAD DE GESTIÓN EDUCATIVA LOCAL Nº304 </h3>
							<address>
								<div class="direccion">Av. Circunvalación s/n de Chuquibambilla   <br>
                     <br>
                    Grau,Apurímac - Perú
                </div>
								<div class="central">Central Telefónica: <a href="tel:973818169" title="Central Telefónica" class="es_link">RPC. 973818169</a>
                </div>
								<div class="correo">Correo: <a href="mailto:informaciones@ugelgrau.gob.pe" title="Correo Principal">ugelgrau304@gmail.com</a></div>
							</address>
						</div>
					</div>

				</div>
				<div class="col-md-3">
					<div class="FooterB">
						<span>Si desea hacer algún comentario o queja puede usar nuestro Libro de Reclamaciones Virtual:</span>
						<a href="#" title="Libro de Reclamaciones" class="libro_reclamacion"></a>
					</div>
				</div>
				
			</div>
		</div>
	</div>

</footer>

<script>window._popup_data = {"ajaxurl":"https:\/\/www.ugel01.gob.pe\/wp-admin\/admin-ajax.php","do":"get_data","ajax_data":{"orig_request_uri":"\/"}};
</script>
<div class="revsliderstyles"><style type="text/css"></style>
</div>
<script type="text/javascript" src="{{asset('plugins/wp-content/plugins/contact-from-7/js/jquery.form.min.js')}}"></script>

<script type="text/javascript" src="{{asset('plugins/wp-content/plugins/contact-from-7/js/scripts.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/wp-content/plugins/wordpress/in/js/wpmu-ui.3.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/wp-content/plugins/wordpress/js/public.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/wp-includes/wp-embed.min.js')}}"></script>

<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-66747607-17', 'auto');
	ga('send', 'pageview');

</script>

<div class="wpmui-overlay">
  
</div>
</body>
</html>